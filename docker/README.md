# Use Asqatasun with Docker

## Who are those dockers for?

Here you'll find docker compositions **for developers of Asqatasun**. This means, you'll have to first build Asqatasun,
and then build your Docker images. Benefit from those docker compositions: you'll have the bleeding edge of
Asqatasun source code (i.e. `master` branch).

If you don't want to build Asqatasun, but do want to use it with Docker, please go the dedicated
repository: <https://gitlab.com/asqatasun/asqatasun-docker>

## Available Docker compositions

- [Asqatasun **server** + **UI**](Asqatasun-MariaDB)

