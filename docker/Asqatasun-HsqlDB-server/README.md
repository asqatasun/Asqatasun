# Docker Asqatasun "all" (server + webapp)

## 0. Prerequisites

- All [pre-requisites to build Asqatasun](https://doc.asqatasun.org/v6/Developer/Build/Pre-requisites/)
- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least)
- [Docker-Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least)

## 1. Build Asqatasun

[Build Asqatasun](https://doc.asqatasun.org/v6/Developer/Build/Build_CLI/)

## 2. Run docker images

Only Asqatasun server:

```shell
docker compose up
```

## 3. Usage

You may browse:

- <http://localhost:8081/> Asqatasun Server API (Swagger UI)

Credentials are:

- login: `admin@asqatasun.org`
- password: `myAsqaPassword`

## Locally override variables

If you want to locally override variables, you should define them as shell variables.

Example: you want to have the MariaDB container reachable from the host port 3307 instead of the usual 3306 (maybe
because you already have a local MariaDB). The port published on the host is defined by variable `DB_HOST_PORT`. Here's
what you should do:

```shell
DB_HOST_PORT=3307 docker compose up
```
