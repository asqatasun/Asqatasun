/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 * 
 *  This file is part of Asqatasun.
 * 
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.textbuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author jkowalczyk
 */
public class DeepTextElementBuilderTest {
    
    private static final Logger LOGGER =
            LoggerFactory.getLogger(DeepTextElementBuilderTest.class);

    /**
     * Test of buildTextFromElement method, of class LinkTextElementBuilder.
     */
    @Test
    public void testExtremeDeepLink() {
        LOGGER.debug("buildTextFromElement of Extreme Deep Link");
        Document document = Jsoup.parse("<a href=\"https://www.tt24.fr/24h-france-1\"> <small> <small> <small> <span style=\"font-size: 10pt; color: rgb(102, 51, 102); font-family: Segoe UI;\"> <span style=\"font-family: Times New Roman,Times,serif;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-weight: bold; color: rgb(51, 0, 51);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-weight: bold; color: rgb(51, 0, 51);\"> <span style=\"font-weight: bold; color: rgb(51, 0, 51);\"> <span style=\"color: rgb(51, 0, 51);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-weight: bold;\"> <span style=\"font-weight: bold; color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <b> <span style=\"font-size: 7.5pt; font-family: &quot;Segoe UI&quot;,sans-serif; color: rgb(102, 51, 102);\"></span> </b> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-size: 10pt; color: rgb(102, 51, 102); font-family: Segoe UI; font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-size: 10pt; color: rgb(102, 51, 102); font-family: Segoe UI; font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-size: 10pt; color: rgb(102, 51, 102); font-family: Segoe UI; font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"font-size: 10pt; color: rgb(102, 51, 102); font-family: Segoe UI; font-weight: bold;\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <span style=\"color: rgb(102, 51, 102);\"> <b> <span style=\"font-size: 10pt; font-family: &quot;Segoe UI&quot;,sans-serif; color: rgb(102, 51, 102);\"></span> </b> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> <b> <span style=\"background: rgb(43, 173, 216) none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: white;\"> <span style=\"background: rgb(43, 173, 216) none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: white;\"> Activer le</span> <span style=\"background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: white;\"> </span> <span style=\"background: white none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: rgb(102, 51, 102);\">lien vers</span> <span style=\"background: rgb(207, 0, 0) none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: rgb(102, 51, 102);\"> </span> <span style=\"background: rgb(207, 0, 0) none repeat scroll 0% 50%; -moz-background-clip: initial; -moz-background-origin: initial; -moz-background-inline-policy: initial; color: white;\">la page </span> </span> </b> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </span> </small> </small> </small> <br></a>");
        Element el = document.select("a").first();
        DeepTextElementBuilder instance = new DeepTextElementBuilder();
        String expResult = "Activer le lien vers la page";
        String result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);
    }

    @Test
    public void testNestedLink() {
        LOGGER.debug("buildTextFromElement of Nested Link");
        Document document = Jsoup.parse("<div role=\"link\"><span>cliquez</span><span>ici</span> </div>");
        Element el = document.select("[role=\"link\"]").first();
        DeepTextElementBuilder instance = new DeepTextElementBuilder();
        String expResult = "cliquez ici";
        String result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);
    }


    @Test
    public void testCompositeLink() {
        LOGGER.debug("buildTextFromElement of Composite Link");
        Document document = Jsoup.parse("<a href=\"mock-link.html\">\n" +
            "        <img src=\"mock-image1.jpg\" alt=\"En \"/>\n" +
            "        <img src=\"mock-image2.jpg\" alt=\" savoir\"/>\n" +
            "    plus\n" +
            "        </a>");
        Element el = document.select("a").first();
        DeepTextElementBuilder instance = new DeepTextElementBuilder();
        String expResult = "En savoir plus";
        String result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);

        document = Jsoup.parse("<a href=\"mock-link.html\">\n" +
            "        <object type=\"image/…\" data=\"mock-image.jpg\">En</object>\n" +
            "    savoir\n" +
            "        <object type=\"image/…\" data=\"mock-image.jpg\"> plus</object>\n" +
            "    </a>");
        el = document.select("a").first();
        expResult = "En savoir plus";
        result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);

        document = Jsoup.parse("<a href=\"my-link.html\">\n" +
            "        <canvas>\n" +
            "    En\n" +
            "        </canvas>\n" +
            "    savoir\n" +
            "        <canvas>\n" +
            "        plus\n" +
            "        </canvas>\n" +
            "    </a>");
        el = document.select("a").first();
        expResult = "En savoir plus";
        result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);

        document = Jsoup.parse("<a href=\"my-link7.html\">\n" +
            "        <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 300 100\">\n" +
            "            <circle cx=\"50\" cy=\"50\" r=\"40\"/>\n" +
            "    <title>En savoir</title>\n" +
            "        </svg>\n" +
            "        <span>plus</span>\n" +
            "    </a>");
        el = document.select("a").first();
        expResult = "En savoir plus";
        result = instance.buildTextFromElement(el);
        assertEquals(expResult, result);
    }

}
