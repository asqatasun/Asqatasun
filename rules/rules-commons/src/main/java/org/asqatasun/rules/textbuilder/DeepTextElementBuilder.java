/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 * 
 *  This file is part of Asqatasun.
 * 
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.textbuilder;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.List;
import java.util.stream.Collectors;

import static org.asqatasun.rules.keystore.AttributeStore.ALT_ATTR;
import static org.asqatasun.rules.keystore.HtmlElementStore.AREA_ELEMENT;
import static org.asqatasun.rules.keystore.HtmlElementStore.IMG_ELEMENT;

/**
 * This implementation of the {@link TextualElementBuilder} extracts the 
 * text of an element by calling recursively the tag children and by adding
 * the content of the alt attribute of tags when they exists.
 */
public class DeepTextElementBuilder implements TextElementBuilder{

    private final TextElementBuilder altAttrTextBuilder = new TextAttributeOfElementBuilder(ALT_ATTR);

    @Override
    public String buildTextFromElement(Element element) {
        return buildText(element)
            .stream()
            .filter(text -> !text.isBlank())
            .map(StringUtils::trim)
            .collect(Collectors.joining(String.valueOf(SPACER)));
    }

    private List<String> buildText(Node node) {
        if (node instanceof TextNode && !((TextNode) node).isBlank()) {
            // Base case: return the trimmed text of TextNode
            return List.of(((TextNode) node).text());
        } else if (node instanceof Element && node.hasAttr(ALT_ATTR) &&
            (((Element) node).tag().normalName().equals(IMG_ELEMENT) ||
                ((Element) node).tag().normalName().equals(AREA_ELEMENT))) {
            // Handle img and area tags with "alt" attribute
            return List.of(altAttrTextBuilder.buildTextFromElement((Element) node));
        } else {
            // Recursive case: process child nodes
            return node.childNodes().stream()
                .flatMap(child -> buildText(child).stream())
                .collect(Collectors.toList());
        }
    }
    
}
