/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 * 
 *  This file is part of Asqatasun.
 * 
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.textbuilder;

import org.apache.commons.collections4.CollectionUtils;
import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * This implementation of the {@link TextualElementBuilder} extracts the text
 * content of a given child of an element. The child element type is settable by
 * contructor argument
 */
public class TextChildOfElementBuilder implements TextElementBuilder {

    /* the attribute name*/
    private final Collection<String> childNames = new HashSet<>();
    public final void setAttributeName(String childName) {
        this.childNames.add(childName);
    }

    /**
     * Constructor
     */
    public TextChildOfElementBuilder() {
        super();
    }


    /**
     * Constructor
     * @param childNames
     */
    public TextChildOfElementBuilder(String... childNames) {
        this.childNames.addAll(Arrays.asList(childNames));
    }

    /**
     * 
     * @param element
     * @return the content of the attribute when it exits, null instead.
     */
    @Override
    public String buildTextFromElement(Element element) {
        if (CollectionUtils.isEmpty(childNames)) {
            return null;
        }

        StringBuilder strb = new StringBuilder();

        childNames.stream()
            .map(childName -> element.children().select(childName))
            .filter(children -> !children.isEmpty())
            .forEach(children -> children.forEach(child -> {
                strb.append(child.text()).append(SPACER);
            }));

        String result = strb.toString().trim();

        return result.isEmpty() ? null : result;
    }

}
