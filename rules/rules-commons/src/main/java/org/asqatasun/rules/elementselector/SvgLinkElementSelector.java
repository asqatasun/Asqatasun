/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 * 
 *  This file is part of Asqatasun.
 * 
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */

package org.asqatasun.rules.elementselector;

import org.asqatasun.rules.textbuilder.TextElementBuilder;

import static org.asqatasun.rules.keystore.CssLikeQueryStore.SVG_LINK_CSS_LIKE_QUERY;

/**
 * Element selector implementation that select svg links. 
 * An svg link is a composite link that only contains one svg tag. 
 * 
 * @author jkowalczyk
 */
public class SvgLinkElementSelector extends LinkElementSelector {

    /**
     * Constructor
     * @param considerContext
     */
    public SvgLinkElementSelector(boolean considerContext) {
        super(considerContext);
    }

    public SvgLinkElementSelector(boolean considerContext, TextElementBuilder linkTextElementBuilder) {
        super(considerContext, linkTextElementBuilder);
    }

    /**
     * Constructor
     * @param considerTitleAsContext
     * @param considerContext
     */
    public SvgLinkElementSelector(
            boolean considerTitleAsContext, 
            boolean considerContext) {
        super(considerTitleAsContext, considerContext);
    }
    
    @Override
    protected String getCssLikeQuery() {
        return SVG_LINK_CSS_LIKE_QUERY;
    }

}
