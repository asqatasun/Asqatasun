/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.elementchecker.lang.detector;

import com.github.pemistahl.lingua.api.Language;
import com.github.pemistahl.lingua.api.LanguageDetectorBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

/**
 * Utility class, implementing the singleton pattern, that enables to 
 * determine the language of some text.
 */
public class LanguageDetector {

    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageDetector.class);

    private final com.github.pemistahl.lingua.api.LanguageDetector detector;
    /**
     * The holder that handles the unique instance of LanguageDetector
     */
    private static class LanguageDetectorHolder {
        private static final LanguageDetector INSTANCE = new LanguageDetector();
    }
    
    /**
     * Private constructor
     */
    private LanguageDetector() {
        this.detector = LanguageDetectorBuilder.fromAllSpokenLanguages().build();
    }
    
    /**
     * Singleton pattern based on the "Initialization-on-demand 
     * holder idiom". See @http://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
     * @return the unique instance of LanguageDetector
     */
    public static LanguageDetector getInstance() {
        return LanguageDetectorHolder.INSTANCE;
    }

    /**
     * Perform the detection 
     * 
     * @param text to test
     * @return the detected language
     */
    public LanguageDetectionResult detectLanguage(String text) {

        Map<Language, Double> detectedLanguages = detector.computeLanguageConfidenceValues(text.toLowerCase());

        Optional<Map.Entry<Language, Double>> maxEntry = detectedLanguages.entrySet()
            .stream()
            .max(Map.Entry.comparingByValue());

        return maxEntry
            .map(entry -> new LanguageDetectionResult(entry.getKey(), entry.getValue(), text))
            .orElse(null);

    }
    
}
