package org.asqatasun.rules.rgaa412;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.asqatasun.ruleimplementation.AbstractMarkerPageRuleImplementation;
import org.asqatasun.rules.elementchecker.CompositeChecker;
import org.asqatasun.rules.elementchecker.attribute.AttributeWithValuePresenceChecker;
import org.asqatasun.rules.elementchecker.element.AccessibleNamePresenceChecker;
import org.asqatasun.rules.elementselector.ElementSelector;

import static org.asqatasun.entity.audit.TestSolution.*;
import static org.asqatasun.rules.keystore.HtmlElementStore.IMG_ELEMENT;
import static org.asqatasun.rules.keystore.AttributeStore.ROLE_ATTR;
import static org.asqatasun.rules.keystore.MarkerStore.DECORATIVE_IMAGE_MARKER;
import static org.asqatasun.rules.keystore.MarkerStore.INFORMATIVE_IMAGE_MARKER;
import static org.asqatasun.rules.keystore.RemarkMessageStore.*;

public class AbstractInformativeImagePresenceAlternativePageRuleImplementation extends AbstractMarkerPageRuleImplementation {

    public AbstractInformativeImagePresenceAlternativePageRuleImplementation(
        ElementSelector elementSelector,
        boolean failInCaseOfAbsence,
        String... eeList) {
        super(elementSelector,
            // the informative images are part of the scope
            INFORMATIVE_IMAGE_MARKER,
            // the decorative images are not part of the scope
            DECORATIVE_IMAGE_MARKER,
            new CompositeChecker(
                false,
                FAILED,
                new AttributeWithValuePresenceChecker(
                    ROLE_ATTR,
                    IMG_ELEMENT,
                    // is already use above in this case.
                    new ImmutablePair<>(PASSED, null),
                    new ImmutablePair<>(FAILED, ARIA_IMG_ROLE_MISSING_MSG),
                    eeList
                ),
                new AccessibleNamePresenceChecker(
                    new ImmutablePair<>(PASSED, null),
                    failInCaseOfAbsence? new ImmutablePair<>(FAILED, ALT_MISSING_MSG) : new ImmutablePair<>(NEED_MORE_INFO, CHECK_PRESENCE_OF_ALTERNATIVE_MECHANISM_FOR_INFORMATIVE_IMG_MSG),
                    eeList
                )
            ),
            new AccessibleNamePresenceChecker(
                new ImmutablePair<>(NEED_MORE_INFO, CHECK_NATURE_OF_IMAGE_WITH_TEXTUAL_ALTERNATIVE_MSG),
                new ImmutablePair<>(NEED_MORE_INFO, CHECK_NATURE_OF_IMAGE_WITHOUT_TEXTUAL_ALTERNATIVE_MSG),
                eeList));
    }
}
