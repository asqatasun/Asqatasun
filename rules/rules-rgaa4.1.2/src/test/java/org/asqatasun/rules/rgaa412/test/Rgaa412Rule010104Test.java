/**
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.rgaa412.test;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.asqatasun.entity.audit.ProcessResult;
import org.asqatasun.entity.audit.TestSolution;
import org.asqatasun.rules.keystore.HtmlElementStore;

import static org.asqatasun.rules.keystore.AttributeStore.ALT_ATTR;
import static org.asqatasun.rules.keystore.AttributeStore.SRC_ATTR;
import static org.asqatasun.rules.keystore.RemarkMessageStore.CHECK_LINK_ASSO_WITH_SERVER_SIDED_IMG_MAP;

/**
 * Unit test class for implementation of rule 1.1.4 (referential RGAA 4.1.2)
 *
 * For more details about implementation, refer to <a href="https://doc.asqatasun.org/v6/en/Business-rules/RGAA-v4.1.2/01.Images/Rule-1-1-4.md">rule 1.1.4 design page</a>.
 * @see <a href="https://accessibilite.numerique.gouv.fr/methode/criteres-et-tests/#1.1.4">1.1.4 rule specification</a>
 */
public class Rgaa412Rule010104Test extends Rgaa412RuleImplementationTestCase {

    /**
     * Default constructor
     * @param testName
     */
    public Rgaa412Rule010104Test(String testName) {
        super(testName);
    }

    @Override
    protected void setUpRuleImplementationClassName() {
        setRuleImplementationClassName("org.asqatasun.rules.rgaa412.Rgaa412Rule010104");
    }

    @Override
    protected void setUpWebResourceMap() {
        addWebResource("Rgaa412.Test.1.1.4-3NMI-01");
        addWebResource("Rgaa412.Test.1.1.4-4NA-01");
    }

    @Override
    protected void setProcess() {
        //----------------------------------------------------------------------
        //------------------------------3NMI-01---------------------------------
        //----------------------------------------------------------------------
        ProcessResult processResult = processPageTest("Rgaa412.Test.1.1.4-3NMI-01");
        checkResultIsPreQualified(processResult, 2, 2);
        checkRemarkIsPresent(
            processResult,
            TestSolution.NEED_MORE_INFO,
            CHECK_LINK_ASSO_WITH_SERVER_SIDED_IMG_MAP,
            HtmlElementStore.IMG_ELEMENT,
            1,
            new ImmutablePair<>(ALT_ATTR, "Map"),
            new ImmutablePair<>(SRC_ATTR, "mock-image-ismap-presence.jpg"));
        checkRemarkIsPresent(
            processResult,
            TestSolution.NEED_MORE_INFO,
            CHECK_LINK_ASSO_WITH_SERVER_SIDED_IMG_MAP,
            HtmlElementStore.INPUT_ELEMENT,
            2,
            new ImmutablePair(ALT_ATTR, "Map"),
            new ImmutablePair(SRC_ATTR, "mock-image-ismap-presence.jpg"));


        //----------------------------------------------------------------------
        //------------------------------4NA-01------------------------------
        //----------------------------------------------------------------------
        checkResultIsNotApplicable(processPageTest("Rgaa412.Test.1.1.4-4NA-01"));
    }

}
