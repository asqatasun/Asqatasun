/**
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.rgaa412.test;

import org.asqatasun.entity.audit.ProcessResult;
import org.asqatasun.entity.audit.TestSolution;

/**
 * Unit test class for implementation of rule 5.6.4 (referential RGAA 4.1.2)
 *
 * For more details about implementation, refer to <a href="https://doc.asqatasun.org/v6/en/Business-rules/RGAA-v4.1.2/05.Tables/Rule-5-6-4.md">rule 5.6.3 design page</a>.
 * @see <a href="https://accessibilite.numerique.gouv.fr/methode/criteres-et-tests/#5.6.4">5.6.4 rule specification</a>
 */
public class Rgaa412Rule050604Test extends Rgaa412RuleImplementationTestCase {

    /**
     * Default constructor
     * @param testName
     */
    public Rgaa412Rule050604Test(String testName) {
        super(testName);
    }

    @Override
    protected void setUpRuleImplementationClassName() {
        setRuleImplementationClassName(
            "org.asqatasun.rules.rgaa412.Rgaa412Rule050604");
    }

    @Override
    protected void setUpWebResourceMap() {
        addWebResource("Rgaa412.Test.5.6.3-5NT-01");
    }

    @Override
    protected void setProcess() {
        //----------------------------------------------------------------------
        //------------------------------5NT-01---------------------------------
        //----------------------------------------------------------------------
        ProcessResult processResult = processPageTest("Rgaa412.Test.5.6.3-5NT-01");
        checkResultIsNotTested(processResult); // temporary result to make the result buildable before implementation
    }

    @Override
    protected void setConsolidate() {

        // The consolidate method can be removed when real implementation is done.
        // The assertions are automatically tested regarding the file names by
        // the abstract parent class
        assertEquals(TestSolution.NOT_TESTED,
            consolidate("Rgaa412.Test.5.6.3-5NT-01").getValue());
    }

}
