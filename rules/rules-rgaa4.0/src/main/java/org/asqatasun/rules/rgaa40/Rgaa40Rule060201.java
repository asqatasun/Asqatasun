/**
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.rgaa40;

import org.asqatasun.entity.audit.TestSolution;
import org.asqatasun.ruleimplementation.AbstractPageRuleWithSelectorAndCheckerImplementation;
import org.asqatasun.rules.elementchecker.text.TextEmptinessChecker;
import org.asqatasun.rules.elementselector.MultipleElementSelector;
import org.asqatasun.rules.textbuilder.LinkTextElementBuilder;

import static org.asqatasun.rules.keystore.AttributeStore.TITLE_ATTR;
import static org.asqatasun.rules.keystore.CssLikeQueryStore.LINK_WITH_HREF_CSS_LIKE_QUERY;
import static org.asqatasun.rules.keystore.CssLikeQueryStore.ROLE_LINK_CSS_LIKE_QUERY;
import static org.asqatasun.rules.keystore.EvidenceStore.COMPUTED_LINK_TITLE;
import static org.asqatasun.rules.keystore.RemarkMessageStore.EMPTY_LINK_MSG;

/**
 * Implementation of rule 6.2.1 (referential RGAA 4.0)
 *
 * For more details about implementation, refer to <a href="https://doc.asqatasun.org/v5/en/Business-rules/RGAA-v4/06.Links/Rule-6-2-1/">rule 6.2.1 design page</a>.
 * @see <a href="https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-6-2-1">6.2.1 rule specification</a>
 */
public class Rgaa40Rule060201 extends AbstractPageRuleWithSelectorAndCheckerImplementation {

    /**
     * Default constructor
     */
    public Rgaa40Rule060201() {
        super(
            new MultipleElementSelector(LINK_WITH_HREF_CSS_LIKE_QUERY, ROLE_LINK_CSS_LIKE_QUERY),
            new TextEmptinessChecker(
                new LinkTextElementBuilder(),
                TestSolution.FAILED,
                TestSolution.PASSED,
                // message associated with link with empty link title
                EMPTY_LINK_MSG,
                // no message when passed
                null,
                //evidence elements
                COMPUTED_LINK_TITLE,
                TITLE_ATTR));
    }

}
