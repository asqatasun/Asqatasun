package org.asqatasun.web.contract

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.asqatasun.model.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.ErrorResponse
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("api/v0/contract")
class ContractController(private val contractService: ContractService) {

    @Tag(name = "Contract", description = "Contract of any kind")
    @Operation(summary = "Get all contracts")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')"
        + "or authentication.principal.username.equals(#username) ")
    fun getAll(@RequestParam username :String?) = contractService.getAllContract(username)

    @Tag(name = "Contract", description = "Contract of any kind")
    @Operation(summary = "Get a contract by its id")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    @PostAuthorize(value = "hasAuthority('ROLE_ADMIN')"
        + "or authentication.principal.username.equals(returnObject.user.username) ")
    fun get(@PathVariable id: Long) = contractService.getContractFromId(id)

    @Tag(name = "Contract", description = "Contract of any kind")
    @Operation(summary = "Delete a contract by its id (only for ADMIN users)")
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    @PostAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    fun delete(@PathVariable id: Long) = contractService.deleteContractFromId(id)


    @Tag(name = "Contract", description = "Contract of any kind")
    @Operation(summary = "Add a contract (only for ADMIN users)")
    @ResponseStatus(HttpStatus.CREATED)
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @Throws(Exception::class)
    fun add(@RequestBody contract: ContractAddDto) = contractService.addContract(contract)


    @Tag(name = "Contract", description = "Contract of any kind")
    @Operation(summary = "Update a contract (only for ADMIN users)")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    fun update(@RequestBody contract: ContractUpdateDto) = contractService.updateContract(contract)

    @ExceptionHandler(
        UserNotFoundException::class,
        ContractNotFoundException::class,
        FunctionalityNotFoundException::class,
        OptionNotFoundException::class,
        ReferentialNotFoundException::class,
        HttpMessageNotReadableException::class)
    fun handleNoResultException(ex: Exception) =
        ErrorResponse.builder(ex, HttpStatus.BAD_REQUEST, ex.message!!).build();

}
