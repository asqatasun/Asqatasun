/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2023  Asqatasun.org
 *
 *  This file is part of Asqatasun.
 *
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.model

import io.swagger.v3.oas.annotations.media.Schema

@Schema(
    description = "Referential against which the audit is ran.",
    example = "RGAA_4_1_2",
)
enum class Referential(val code: String) {
    RGAA_4_0("Rgaa40"),
    RGAA_4_1_2("Rgaa412"),
    SEO("Seo");

    companion object {
        private val map = entries.associateBy(Referential::code)
        fun fromCode(code: String): Referential? = map[code]
    }
}

@Schema(
    description = "Referential level",
    example = "AA",
)
enum class Level(val code: String) {
    AAA("Or"),
    AA("Ar"),
    A("Bz");

    companion object {
        private val map = entries.associateBy(Level::code)
        fun fromCode(code: String): Level? = map[code]
    }
}
