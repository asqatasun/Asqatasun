package org.asqatasun.model


import io.swagger.v3.oas.annotations.media.Schema
import org.asqatasun.entity.user.User

data class UserDto (
    @field:Schema(
        description = "Unique id of the contract",
        example = "42",
    )
    val id: Long,
    val username: String,
    val firstName: String,
    val lastName: String,
    val role: String,
    val isActivated: Boolean
)


fun User.toUserDto() = UserDto (
    id,
    username = email1,
    firstName,
    name,
    role.roleName,
    isAccountActivated
)
