package org.asqatasun.model

import io.swagger.v3.oas.annotations.media.Schema
import org.asqatasun.entity.contract.Act
import java.util.*

data class ActDto (
    @field:Schema(
        description = "Unique id of the act",
        example = "42",
    )
    val id: Long,
    val beginDate: Date,
    val endDate: Date?,
    val status: String,
    val scope: String,
    val clientIp: String?
)
fun Act.toActDto() = ActDto (
    id,
    beginDate,
    endDate,
    status.name,
    scope.label,
    clientIp
)
