function extractDoctype() {
    let node = document.doctype;
    if (node) {
        let html = "<!DOCTYPE "
            + node.name
            + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '')
            + (!node.publicId && node.systemId ? ' SYSTEM' : '')
            + (node.systemId ? ' "' + node.systemId + '"' : '')
            + '>';
        return html;
    }
    return '';
}

return extractDoctype();
