/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.entity.dao.statistics;

import org.asqatasun.entity.audit.TestSolution;
import org.asqatasun.entity.dao.GenericDAO;
import org.asqatasun.entity.parameterization.Parameter;
import org.asqatasun.entity.statistics.WebResourceStatistics;
import org.asqatasun.entity.subject.WebResource;

import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * @author jkowalczyk
 */
public interface WebResourceStatisticsDAO extends GenericDAO<WebResourceStatistics, Long>{

    /**
     *
     * @return
     */
    Class<? extends WebResource> getWebResourceEntityClass();

    /**
     * 
     * @param webresourceId
     * @param testSolution
     * @return
     */
    Long findResultCountByResultType(
            Long webresourceId,
            TestSolution testSolution);

    /**
     * 
     * @param webresourceId
     * @param paramSet
     * @param testSolution
     * @return 
     */
    public BigDecimal findWeightedResultCountByResultType(
            Long webresourceId,
            Collection<Parameter> paramSet,
            TestSolution testSolution);
    
    /**
     * 
     * @param webresourceId
     * @param testSolution
     * @return
     */
    Long findNumberOfOccurrencesByWebResourceAndResultType(Long webresourceId, TestSolution testSolution);

    /**
     * 
     * @param webresourceId
     * @return
     */
    Integer findHttpStatusCodeByWebResource(Long webresourceId);

    /**
     *
     * @param webResource
     * @return
     */
    WebResourceStatistics findWebResourceStatisticsByWebResource(WebResource webResource);
    
    

}
