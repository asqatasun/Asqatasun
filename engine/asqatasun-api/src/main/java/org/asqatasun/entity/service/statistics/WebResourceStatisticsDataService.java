/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.entity.service.statistics;

import org.asqatasun.entity.audit.TestSolution;
import org.asqatasun.entity.parameterization.Parameter;
import org.asqatasun.entity.service.GenericDataService;
import org.asqatasun.entity.statistics.WebResourceStatistics;
import org.asqatasun.entity.subject.WebResource;

import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * @author jkowalczyk
 */
public interface WebResourceStatisticsDataService
        extends GenericDataService<WebResourceStatistics, Long>{

    /**
     * 
     * @param webresourceId
     * @param testSolution
     * @return
     */
    Long getResultCountByResultType(
            Long webresourceId,
            TestSolution testSolution);
    
    /**
     * 
     * @param webresourceId
     * @param paramSet
     * @param testSolution
     * @return
     */
    BigDecimal getWeightedResultByResultType(
            Long webresourceId,
            Collection<Parameter> paramSet,
            TestSolution testSolution);

    /**
     * 
     * @param webresourceId
     * @param testSolution
     * @return
     */
    Long getNumberOfOccurrencesByWebResourceAndResultType(
            Long webresourceId,
            TestSolution testSolution);

    /**
     *
     * @param webresourceId
     * @return
     */
    Integer getHttpStatusCodeByWebResource(Long webresourceId);
    
    /**
     *
     * @param webResource
     * @return
     */
    WebResourceStatistics getWebResourceStatisticsByWebResource(WebResource webResource);

}
