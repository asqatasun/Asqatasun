package org.asqatasun.ruleimplementation;

import org.asqatasun.util.http.HttpRequestHandler;

public interface UrlCheckRule {

    void setHttpRequestHandler(HttpRequestHandler httpRequestHandler);

}
