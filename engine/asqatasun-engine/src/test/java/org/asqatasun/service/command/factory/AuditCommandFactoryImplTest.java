/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 * 
 *  This file is part of Asqatasun.
 * 
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.service.command.factory;

import org.asqatasun.entity.audit.Audit;
import org.asqatasun.entity.audit.AuditStatus;
import org.asqatasun.entity.parameterization.Parameter;
import org.asqatasun.entity.parameterization.ParameterElement;
import org.asqatasun.entity.service.audit.AuditDataService;
import org.asqatasun.entity.service.parameterization.ParameterDataService;
import org.asqatasun.entity.service.parameterization.ParameterElementDataService;
import org.asqatasun.service.command.*;
import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;

/**
 *
 * @author jkowalczyk
 */
public class AuditCommandFactoryImplTest {
    
    private AuditCommandFactoryImpl auditCommandFactory;
    
    private Audit mockAudit;

    private AuditDataService mockAuditDataService;

    @BeforeEach
    protected void setUp() {
        
        mockAuditDataService = EasyMock.createMock(AuditDataService.class);
        ParameterElementDataService mockParameterElementDataService = EasyMock.createMock(ParameterElementDataService.class);
        ParameterDataService mockParameterDataService = EasyMock.createMock(ParameterDataService.class);
        mockAudit = EasyMock.createMock(Audit.class);
        ParameterElement mockAllUrlDuplicationParameterElement = EasyMock.createMock(ParameterElement.class);
        Parameter mockAllUrlDuplicationParameter = EasyMock.createMock(Parameter.class);

        EasyMock.expect(mockAuditDataService.create()).andReturn(mockAudit);
        mockAudit.setStatus(AuditStatus.PENDING);
        EasyMock.expectLastCall().once();
        EasyMock.expect(mockAuditDataService.saveOrUpdate(mockAudit)).andReturn(mockAudit).once();

        EasyMock.expect(mockParameterElementDataService
            .getParameterElement("ALLOW_URL_DUPLICATION_IN_AUDIT"))
            .andReturn(mockAllUrlDuplicationParameterElement);

        EasyMock.expect(mockParameterDataService.getParameter(mockAllUrlDuplicationParameterElement, "true"))
            .andReturn(mockAllUrlDuplicationParameter).once();

        EasyMock.replay(mockAuditDataService);
        EasyMock.replay(mockAudit);
        EasyMock.replay(mockParameterElementDataService);
        EasyMock.replay(mockParameterDataService);

        auditCommandFactory = new AuditCommandFactoryImpl(
            mockAuditDataService,
            null,
            null,
            null,
            mockParameterDataService,
            mockParameterElementDataService,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null);
    }


    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_3args_1_2() {
        System.out.println("create PageAuditCommand without crawler");
        AuditCommand result = this.auditCommandFactory.create(
            "",
            new HashSet<>(),
            new ArrayList<>(),
            false);
        assertInstanceOf(PageAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }
    
    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_3args_2() {
        System.out.println("create SiteAuditCommand");

        AuditCommand result = this.auditCommandFactory.create(
            "",
            new HashSet<>(),
            new ArrayList<>(),
            true);
        assertInstanceOf(SiteAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }

    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_Map_Set() {
        System.out.println("create UploadAuditCommand");

        AuditCommand result = this.auditCommandFactory.create(
            new HashMap<>(),
            new HashSet<>(),
            new ArrayList<>());
        assertInstanceOf(UploadAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }

    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_3args_3_1() {
        System.out.println("create GroupOfPagesAuditCommand with crawler");

        AuditCommand result = this.auditCommandFactory.create(
            "",
            new ArrayList<>(),
            new HashSet<>(),
            new ArrayList<>());
        assertInstanceOf(GroupOfPagesAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }
    
    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_3args_3_2() {
        System.out.println("create GroupOfPagesAuditCommand without crawler");

        AuditCommand result = this.auditCommandFactory.create(
            "https://asqatasun.org",
                List.of("https://asqatasun.org"),
            new HashSet<>(),
            new ArrayList<>());
        assertInstanceOf(GroupOfPagesAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }

    /**
     * Test of create method, of class AuditCommandFactoryImpl.
     */
    @Test
    public void testCreate_3args_4() {
        System.out.println("create ScenarioAuditCommand");

        AuditCommand result = this.auditCommandFactory.create(
            "", "",
            new HashSet<>(),
            new ArrayList<>()
        );
        assertInstanceOf(ScenarioAuditCommandImpl.class, result);
        EasyMock.verify(mockAuditDataService);
        EasyMock.verify(mockAudit);
    }
    
}
