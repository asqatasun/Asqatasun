/*
 *  Asqatasun - Automated webpage assessment
 *  Copyright (C) 2008-2020  Asqatasun.org
 *
 *  This file is part of Asqatasun.
 *
 *  Asqatasun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Contact us by mail: asqatasun AT asqatasun DOT org
 */

package org.asqatasun.service.command;

import jakarta.persistence.PersistenceException;
import org.apache.http.HttpStatus;
import org.asqatasun.analyser.AnalyserService;
import org.asqatasun.consolidator.ConsolidatorService;
import org.asqatasun.contentadapter.AdaptationListener;
import org.asqatasun.entity.audit.*;
import org.asqatasun.entity.contract.ScopeEnum;
import org.asqatasun.entity.parameterization.Parameter;
import org.asqatasun.entity.reference.Test;
import org.asqatasun.entity.service.audit.*;
import org.asqatasun.entity.service.parameterization.ParameterDataService;
import org.asqatasun.entity.service.reference.TestDataService;
import org.asqatasun.entity.service.subject.WebResourceDataService;
import org.asqatasun.entity.subject.Page;
import org.asqatasun.entity.subject.Site;
import org.asqatasun.entity.subject.WebResource;
import org.asqatasun.service.ContentAdapterService;
import org.asqatasun.service.ProcessorService;
import org.asqatasun.service.ScenarioLoaderService;
import org.asqatasun.util.MD5Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static org.asqatasun.entity.contract.ScopeEnum.PAGE;

/**
 * @author jkowalczyk
 */
public abstract class AuditCommandImpl implements AuditCommand {

    public static final String AUDIT_STATUS_IS_LOGGER_STR = " status is ";
    public static final String WHILE_LOGGER_STR = " while ";
    public static final String WAS_REQUIRED_LOGGER_STR = " was required";
    public static final String TO_LOGGER_STR = " to ";
    public static final String MS_LOGGER_STR = " ms ";
    public static final String SSP_TOOK_LOGGER_STR = " SSP took ";
    public static final String CONSOLIDATING_TOOK_LOGGER_STR = "Consolidating took ";
    public static final int DEFAULT_ANALYSE_TREATMENT_WINDOW = 10;
    public static final int DEFAULT_PROCESSING_TREATMENT_WINDOW = 4;
    public static final int DEFAULT_ADAPTATION_TREATMENT_WINDOW = 4;
    public static final int DEFAULT_CONSOLIDATION_TREATMENT_WINDOW = 200;
    private static final Logger LOGGER = LoggerFactory.getLogger(AuditCommandImpl.class);
    /**
     * The audit parameters
     */
    private final Set<Parameter> paramSet;
    protected int adaptationTreatmentWindow = DEFAULT_ADAPTATION_TREATMENT_WINDOW;
    protected int analyseTreatmentWindow = DEFAULT_ANALYSE_TREATMENT_WINDOW;
    protected int consolidationTreatmentWindow = DEFAULT_CONSOLIDATION_TREATMENT_WINDOW;
    protected int processingTreatmentWindow = DEFAULT_PROCESSING_TREATMENT_WINDOW;
    // The dataServices
    protected AuditDataService auditDataService;
    protected TestDataService testDataService;
    protected ParameterDataService parameterDataService;
    protected WebResourceDataService webResourceDataService;
    protected ContentDataService contentDataService;
    protected ProcessResultDataService processResultDataService;
    protected TagDataService tagDataService;
    protected PreProcessResultDataService preProcessResultDataService;
    // The services
    protected ScenarioLoaderService scenarioLoaderService;
    protected ContentAdapterService contentAdapterService;
    protected ProcessorService processorService;
    protected ConsolidatorService consolidatorService;
    protected AnalyserService analyserService;
    // The listeners
    protected AdaptationListener adaptationListener;
    protected ScopeEnum scope;
    protected String targetUrl;
    private Audit audit;
    // the options
    private boolean cleanUpRelatedContent = true;
    /**
     * The audit tags
     */
    private List<Tag> tagList;

    /**
     * @param paramSet
     * @param tagList
     * @param auditDataService
     */
    public AuditCommandImpl(
        Set<Parameter> paramSet,
        List<Tag> tagList,
        AuditDataService auditDataService,
        ScopeEnum scope) {
        this.paramSet = paramSet;
        this.tagList = tagList;
        this.auditDataService = auditDataService;
        this.scope = scope;
        audit = auditDataService.create();
        setStatusToAudit(AuditStatus.PENDING);
    }

    public void setAdaptationTreatmentWindow(int adaptationTreatmentWindow) {
        this.adaptationTreatmentWindow = adaptationTreatmentWindow;
    }

    public void setAnalyseTreatmentWindow(int analyseTreatmentWindow) {
        this.analyseTreatmentWindow = analyseTreatmentWindow;
    }

    public void setConsolidationTreatmentWindow(int consolidationTreatmentWindow) {
        this.consolidationTreatmentWindow = consolidationTreatmentWindow;
    }

    public void setProcessingTreatmentWindow(int processingTreatmentWindow) {
        this.processingTreatmentWindow = processingTreatmentWindow;
    }

    @Override
    public Audit getAudit() {
        return audit;
    }

    @Override
    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    public void setAuditDataService(AuditDataService auditDataService) {
        this.auditDataService = auditDataService;
    }

    public void setTestDataService(TestDataService testDataService) {
        this.testDataService = testDataService;
    }

    public void setParameterDataService(ParameterDataService parameterDataService) {
        this.parameterDataService = parameterDataService;
    }

    public void setWebResourceDataService(WebResourceDataService webResourceDataService) {
        this.webResourceDataService = webResourceDataService;
    }

    public void setContentDataService(ContentDataService contentDataService) {
        this.contentDataService = contentDataService;
    }

    public void setProcessResultDataService(ProcessResultDataService processResultDataService) {
        this.processResultDataService = processResultDataService;
    }

    public void setTagDataService(TagDataService tagDataService) {
        this.tagDataService = tagDataService;
    }

    public void setPreProcessResultDataService(PreProcessResultDataService preProcessResultDataService) {
        this.preProcessResultDataService = preProcessResultDataService;
    }

    public void setScenarioLoaderService(ScenarioLoaderService scenarioLoaderService) {
        this.scenarioLoaderService = scenarioLoaderService;
    }

    public void setContentAdapterService(ContentAdapterService contentAdapterService) {
        this.contentAdapterService = contentAdapterService;
    }

    public void setProcessorService(ProcessorService processorService) {
        this.processorService = processorService;
    }

    public void setConsolidatorService(ConsolidatorService consolidatorService) {
        this.consolidatorService = consolidatorService;
    }

    public void setAnalyserService(AnalyserService analyserService) {
        this.analyserService = analyserService;
    }

    public void setAdaptationListener(AdaptationListener adaptationListener) {
        this.adaptationListener = adaptationListener;
    }

    public void setCleanUpRelatedContent(boolean cleanUpRelatedContent) {
        this.cleanUpRelatedContent = cleanUpRelatedContent;
    }

    @Override
    public void init() {
        LOGGER.info("Audit "
            + this.audit.getId()
            + " initialized, queried URL "
            + this.targetUrl
        );

        // the paramSet has to be persisted
        parameterDataService.saveOrUpdate(paramSet);
        if (!tagList.isEmpty()) {
            tagList = new ArrayList<>(tagDataService.saveOrUpdate(tagList));
            audit.setTagList(tagList);
        }
        audit.setTestList(testDataService.getTestListFromParamSet(paramSet));
        audit.setParameterSet(paramSet);
        setStatusToAudit(AuditStatus.SCENARIO_LOADING);
        LOGGER.info("Audit "
            + this.audit.getId()
            + " ready to load scenario "
            + this.targetUrl
        );
    }

    @Override
    public void crawl() {
        // By default, do nothing
    }

    @Override
    public void loadContent() {
        LOGGER.info("Audit {} Phase 1/5 Loading", audit.getId());
        LOGGER.debug("Loading content for {}", this.targetUrl);
        if (!getAudit().getStatus().equals(AuditStatus.SCENARIO_LOADING)) {
            setStatusToAudit(AuditStatus.ERROR);
            LOGGER.info("Audit " + audit.getId() + " ended, status: " + audit.getStatus());
            return;
        }
        // the returned content list is already persisted and associated with
        // the current audit
        WebResource webResource;
        if (audit.getSubject() != null) {
            webResource = audit.getSubject();
        } else {
            webResource = createWebResource();
        }

        LOGGER.debug("Audit {} Web_resource {}, actual URL {}", this.audit.getId(), webResource.getId(), webResource.getURL());

        callScenarioLoadService(webResource);
        setStatusToAudit(AuditStatus.CONTENT_ADAPTING);

        LOGGER.debug("{} has been loaded", this.targetUrl);
    }

    protected void callScenarioLoadService(WebResource webResource) {
        scenarioLoaderService.loadScenario(webResource);
    }

    @Override
    public void adaptContent() {
        audit = auditDataService.read(audit.getId());
        if (!audit.getStatus().equals(AuditStatus.CONTENT_ADAPTING)) {
            return;
        }

        LOGGER.info("Audit {} Phase 2/5 Adapting", audit.getId());
        LOGGER.debug("Adapting {}", audit.getSubject().getURL());

        // debug tools
        Date endRetrieveDate;
        Date endProcessDate;
        Date endPersistDate;
        Long persistenceDuration = (long) 0;

        boolean hasCorrectDOM = false;
        Long i = (long) 0;
        Long webResourceId = audit.getSubject().getId();
        Long nbOfContent = contentDataService.getNumberOfSSPFromWebResource(audit.getSubject(), HttpStatus.SC_OK);

        // Some actions have to be realized when the adaptation starts
        if (adaptationListener != null) {
            adaptationListener.adaptationStarted(audit);
        }

        while (i.compareTo(nbOfContent) < 0) {

            LOGGER.trace("Adapting ssp from  {}" + TO_LOGGER_STR + "{} for {}", i, i + adaptationTreatmentWindow, audit.getSubject().getURL());

            Collection<Content> contentList = contentDataService.getSSPFromWebResource(
                webResourceId,
                i,
                adaptationTreatmentWindow,
                true);
            endRetrieveDate = Calendar.getInstance().getTime();

            // Set the referential to the contentAdapterService due to different
            // behaviour in the implementation. Has to be removed when accessiweb 2.1
            // implementations will be based on jsoup.
            Set<Content> contentSet = new HashSet<>(contentAdapterService.adaptContent(contentList));

            endProcessDate = Calendar.getInstance().getTime();
            LOGGER.trace("Adapting " + contentList.size() + SSP_TOOK_LOGGER_STR +
                (endProcessDate.getTime() - endRetrieveDate.getTime()) + MS_LOGGER_STR + contentSet.size());

            hasCorrectDOM = hasCorrectDOM || hasContentSetAtLeastOneCorrectDOM(contentSet);

            this.encodeSourceAndPersistContentList(contentSet);

            endPersistDate = Calendar.getInstance().getTime();
            LOGGER.trace("Persisting  {}" + SSP_TOOK_LOGGER_STR + "{}" + MS_LOGGER_STR + "for {}", contentSet.size(), endPersistDate.getTime() - endProcessDate.getTime(), audit.getSubject().getURL());
            persistenceDuration = persistenceDuration + (endPersistDate.getTime() - endProcessDate.getTime());
            i = i + adaptationTreatmentWindow;
            // explicit call of the Gc
            System.gc();
        }

        LOGGER.trace("Application spent {} ms to write in Disk while adapting", persistenceDuration);

        if (hasCorrectDOM) {
            setStatusToAudit(AuditStatus.PROCESSING);
        } else {
            LOGGER.info("Audit {} has no correct DOM", audit.getId());
            setStatusToAudit(AuditStatus.ERROR);
        }

        // Some actions have to be realized when the adaptation is completed
        if (adaptationListener != null) {
            adaptationListener.adaptationCompleted(audit);
        }
        LOGGER.debug("{} has been adapted", audit.getSubject().getURL());
    }

    /**
     * @param contentSet
     * @return
     */
    private boolean hasContentSetAtLeastOneCorrectDOM(Set<Content> contentSet) {
        for (Content content : contentSet) {
            // if one SSP with not empty DOM is encountered, we return true
            if (content instanceof SSP &&
                !((SSP) content).getDOM().isEmpty()) {

                return true;
            }
        }
        return false;
    }

    /**
     * Encode Source code and persist the content list
     *
     * @param contentSet
     */
    private void encodeSourceAndPersistContentList(Set<Content> contentSet) {
        for (Content content : contentSet) {
            if (content instanceof SSP &&
                !((SSP) content).getDOM().isEmpty()) {
                try {
                    ((SSP) content).setSource(MD5Encoder.MD5(((SSP) content).getSource()));
                } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                    LOGGER.warn(ex.getMessage());
                }
            }
            try {
                contentDataService.saveOrUpdate(content);
            } catch (PersistenceException pe) {
                LOGGER.warn("{} could not have been persisted due to {}", content.getURI(), pe.getLocalizedMessage());
                if (content instanceof SSP &&
                    !((SSP) content).getDOM().isEmpty()) {
                    ((SSP) content).setDOM("");
                    contentDataService.saveOrUpdate(content);
                }
            }
        }
    }

    @Override
    public void process() {
        audit = auditDataService.getAuditWithTest(audit.getId());
        if (!audit.getStatus().equals(AuditStatus.PROCESSING)) {
            return;
        }
        LOGGER.info("Audit {} Phase 3/5 Processing", audit.getId());
        LOGGER.debug("Processing {}", audit.getSubject().getURL());
        // debug tools
        Date beginProcessDate;
        Date endProcessDate = null;
        Date endPersistDate;
        long persistenceDuration = 0;

        long i = 0L;
        Long webResourceId = audit.getSubject().getId();
        long nbOfContent = contentDataService.getNumberOfSSPFromWebResource(audit.getSubject(), HttpStatus.SC_OK);

        Set<ProcessResult> processResultSet = new HashSet<>();

        while (i < nbOfContent) {
            LOGGER.trace("Processing from {}" + TO_LOGGER_STR + "{}for {}", i, i + processingTreatmentWindow, audit.getSubject().getURL());
            beginProcessDate = Calendar.getInstance().getTime();
            Collection<Content> contentList = contentDataService.getSSPWithRelatedContentFromWebResource(
                webResourceId,
                i,
                processingTreatmentWindow,
                false);
            processResultSet.clear();
            processResultSet.addAll(processorService.process(contentList, audit.getTestList()));
            for (ProcessResult processResult : processResultSet) {
                processResult.setGrossResultAudit(audit);
            }

            if (LOGGER.isTraceEnabled()) {
                endProcessDate = Calendar.getInstance().getTime();
                LOGGER.trace("Processing of {} elements took {}" + MS_LOGGER_STR + "for {}", processingTreatmentWindow, endProcessDate.getTime() - beginProcessDate.getTime(), audit.getSubject().getURL());
            }
            if (LOGGER.isTraceEnabled()) {
                for (Content content : contentList) {
                    LOGGER.trace("Persisting result for page {}", content.getURI());
                }
            }
            processResultDataService.saveOrUpdate(processResultSet);
            if (LOGGER.isTraceEnabled()) {
                endPersistDate = Calendar.getInstance().getTime();
                LOGGER.trace("Persist processing of {} elements took {}" + MS_LOGGER_STR + "for {}", processingTreatmentWindow, endPersistDate.getTime() - endProcessDate.getTime(), audit.getSubject().getURL());
                persistenceDuration = persistenceDuration
                    + (endPersistDate.getTime() - endProcessDate.getTime());
            }
            i = i + processingTreatmentWindow;
            System.gc();
        }

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Application spent {} ms to write in Disk while processing", persistenceDuration);
        }

        if (processResultDataService.getNumberOfGrossResultFromAudit(audit) > 0) {
            setStatusToAudit(AuditStatus.CONSOLIDATION);
        } else {
            LOGGER.info("Audit {} has no gross result", audit.getId());
            setStatusToAudit(AuditStatus.ERROR);
        }
        LOGGER.trace("{} has been processed", audit.getSubject().getURL());
    }

    @Override
    public void consolidate() {
        audit = auditDataService.getAuditWithTest(audit.getId());
        if (!audit.getStatus().equals(AuditStatus.CONSOLIDATION)) {
            return;
        }

        // debug tools
        Date beginProcessDate = null;
        Date endProcessDate = null;
        Date endPersistDate;
        LOGGER.info("Audit {} Phase 4/5 Consolidating", audit.getId());
        LOGGER.trace("Consolidating {}", audit.getSubject().getURL());
        if (LOGGER.isTraceEnabled()) {
            beginProcessDate = Calendar.getInstance().getTime();
        }
        if (audit.getSubject() instanceof Page) {
            consolidate(processResultDataService.
                getGrossResultFromAudit(audit), audit.getTestList());
            if (LOGGER.isTraceEnabled()) {
                endProcessDate = Calendar.getInstance().getTime();
                LOGGER.trace(CONSOLIDATING_TOOK_LOGGER_STR + (endProcessDate.getTime() - beginProcessDate.getTime()) +
                    MS_LOGGER_STR);
            }
        } else if (audit.getSubject() instanceof Site) {
            if (contentDataService.getNumberOfSSPFromWebResource(audit.getSubject(), HttpStatus.SC_OK) > 20) {
                List<Test> testList = new ArrayList<>();
                for (Test test : audit.getTestList()) {
                    if (!test.getNoProcess()) { // only consolidate if the process has been launched on the test
                        testList.add(test);

                        Collection<ProcessResult> prList = (List<ProcessResult>) processResultDataService.
                            getGrossResultFromAuditAndTest(audit, test);
                        consolidate(prList, testList);
                        testList.clear();
                    }
                }
                if (LOGGER.isTraceEnabled()) {
                    endProcessDate = Calendar.getInstance().getTime();
                    LOGGER.trace(CONSOLIDATING_TOOK_LOGGER_STR +
                        (endProcessDate.getTime() - beginProcessDate.getTime()) + MS_LOGGER_STR);
                }
            } else {
                Collection<ProcessResult> prList = (List<ProcessResult>) processResultDataService.
                    getGrossResultFromAudit(audit);
                consolidate(prList, audit.getTestList());
                if (LOGGER.isTraceEnabled()) {
                    endProcessDate = Calendar.getInstance().getTime();
                    LOGGER.trace(CONSOLIDATING_TOOK_LOGGER_STR +
                        (endProcessDate.getTime() - beginProcessDate.getTime()) + MS_LOGGER_STR);
                }
            }
        }
        audit = auditDataService.saveOrUpdate(audit);
        if (LOGGER.isTraceEnabled()) {
            endPersistDate = Calendar.getInstance().getTime();
            LOGGER.trace("Persisting Consolidation of the audit took{}" + MS_LOGGER_STR, endPersistDate.getTime() - endProcessDate.getTime());
        }
        LOGGER.debug(audit.getSubject().getURL() + " has been consolidated");
    }

    /**
     * @param prList
     * @param testList
     */
    private void consolidate(Collection<ProcessResult> prList, Collection<Test> testList) {
        Set<ProcessResult> processResultSet = new HashSet<>();
        if (LOGGER.isTraceEnabled()) {
            if (testList.size() == 1) {
                LOGGER.trace("Consolidate {} elements for test {}", prList.size(), testList.iterator().next().getCode());
            } else {
                LOGGER.trace("Consolidate {} elements for {} tests ", prList.size(), testList.size());
            }
        }
        processResultSet.addAll(consolidatorService.consolidate(
            prList,
            testList));

        if (!processResultSet.isEmpty()) {
            audit.setStatus(AuditStatus.ANALYSIS);
        } else {
            LOGGER.warn("Audit has no net result");
            audit.setStatus(AuditStatus.ERROR);
        }
        Iterator<ProcessResult> iter = processResultSet.iterator();
        Set<ProcessResult> processResultSubset = new HashSet<>();
        int i = 0;
        while (iter.hasNext()) {
            ProcessResult pr = iter.next();
            pr.setNetResultAudit(audit);
            processResultSubset.add(pr);
            i++;
            if (i % consolidationTreatmentWindow == 0) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Persisting Consolidation from {}" + TO_LOGGER_STR + "{}", i, i + consolidationTreatmentWindow);
                }
                processResultDataService.saveOrUpdate(processResultSubset);
                processResultSubset.clear();
            }
        }
        processResultDataService.saveOrUpdate(processResultSubset);
        processResultSubset.clear();
        System.gc();
    }

    @Override
    public void analyse() {
        audit = auditDataService.read(audit.getId());
        if (!audit.getStatus().equals(AuditStatus.ANALYSIS)) {
            LOGGER.info("Audit " + audit.getId() + " ended, status: " + audit.getStatus());
            return;
        }

        LOGGER.info("Audit {} Phase 5/5 Analyzing", audit.getId());
        LOGGER.debug("Analysing {}", audit.getSubject().getURL());

        // debug tools
        Date beginProcessDate = null;
        Date endProcessDate = null;
        Date endPersistDate;
        Long persistenceDuration = (long) 0;

        WebResource parentWebResource = audit.getSubject();
        if (parentWebResource instanceof Page) {
            analyserService.analyse(parentWebResource, audit);
            webResourceDataService.saveOrUpdate(parentWebResource);
        } else if (parentWebResource instanceof Site) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Analysing results of scope site");
                beginProcessDate = Calendar.getInstance().getTime();
            }
            analyserService.analyse(parentWebResource, audit);
            if (LOGGER.isTraceEnabled()) {
                endProcessDate = Calendar.getInstance().getTime();
                LOGGER.trace("Analysing results of scope site took {}" + MS_LOGGER_STR, endProcessDate.getTime() - beginProcessDate.getTime());
            }
            webResourceDataService.saveOrUpdate(parentWebResource);

            if (LOGGER.isTraceEnabled()) {
                endPersistDate = Calendar.getInstance().getTime();
                LOGGER.trace("Persisting Analysis results of scope site {}" + MS_LOGGER_STR, endPersistDate.getTime() - endProcessDate.getTime());
                persistenceDuration = persistenceDuration
                    + (endPersistDate.getTime() - endProcessDate.getTime());
            }

            Long nbOfContent =
                webResourceDataService.getNumberOfChildWebResource(parentWebResource);
            Long i = (long) 0;
            List<WebResource> webResourceList;
            LOGGER.trace("Analysing {} elements ", nbOfContent);
            while (i.compareTo(nbOfContent) < 0) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Analysing results of scope page from {}" + TO_LOGGER_STR + "{}", i, i + analyseTreatmentWindow);
                    beginProcessDate = Calendar.getInstance().getTime();
                }
                webResourceList = webResourceDataService.getWebResourceFromItsParent(
                    parentWebResource,
                    i.intValue(),
                    analyseTreatmentWindow);
                for (WebResource webResource : webResourceList) {
                    if (LOGGER.isTraceEnabled()) {
                        endProcessDate = Calendar.getInstance().getTime();
                        LOGGER.trace("Analysing results for page {} took {}" + MS_LOGGER_STR, webResource.getURL(), endProcessDate.getTime() - beginProcessDate.getTime());
                    }
                    analyserService.analyse(webResource, audit);
                    if (LOGGER.isTraceEnabled()) {
                        endPersistDate = Calendar.getInstance().getTime();
                        LOGGER.trace("Persisting Analysis results for page {} took {}" + MS_LOGGER_STR, webResource.getURL(), endPersistDate.getTime() - endProcessDate.getTime());
                        persistenceDuration = persistenceDuration
                            + (endPersistDate.getTime() - endProcessDate.getTime());
                    }
                }
                i = i + analyseTreatmentWindow;
            }
        }

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Application spent {} ms to write in Disk while analysing", persistenceDuration);
        }
        LOGGER.debug("{} has been analysed", audit.getSubject().getURL());
        setStatusToAudit(AuditStatus.COMPLETED);
        if (cleanUpRelatedContent) {
            cleanUpTestData(audit);
        }

        LOGGER.info("Audit " + audit.getId() + " ended, status: " + audit.getStatus());
    }

    /**
     * @param audit
     */
    private void cleanUpTestData(Audit audit) {
        LOGGER.debug("Cleaning-up data for {}", audit.getSubject().getURL());
        long i = 0L;
        long webResourceId = audit.getSubject().getId();
        long nbOfContent = contentDataService.getNumberOfSSPFromWebResource(audit.getSubject(), HttpStatus.SC_OK);
        while (i < nbOfContent) {
            Collection<Content> contentList = contentDataService.getSSPWithRelatedContentFromWebResource(
                webResourceId,
                i,
                processingTreatmentWindow,
                true);
            for (Content content : contentList) {
                if (content instanceof SSP) {
                    for (RelatedContent rc : ((SSP) content).getRelatedContentSet()) {
                        contentDataService.delete(rc.getId());
                    }
                }
            }
            i = i + processingTreatmentWindow;
        }
        for (ProcessResult pr : processResultDataService.getIndefiniteResultFromAudit(audit)) {
            processResultDataService.delete(pr.getId());
        }
        for (PreProcessResult ppr : preProcessResultDataService.getPreProcessResultFromAudit(audit)) {
            preProcessResultDataService.delete(ppr.getId());
        }
        LOGGER.debug("Data cleaned-up for {}", audit.getSubject().getURL());
    }

    /**
     * Set a new status to the audit instance and persist it
     *
     * @param auditStatus
     */
    public final void setStatusToAudit(AuditStatus auditStatus) {
        audit.setStatus(auditStatus);
        audit = auditDataService.saveOrUpdate(audit);
    }

    /**
     * Create a webResource of page type and associate it with the current audit
     *
     * @param url
     */
    protected void createEmptyPageResource(String url) {
        Page page = webResourceDataService.createPage(url);
        getAudit().setSubject(page);
        webResourceDataService.saveOrUpdate(page);
    }

    /**
     * Create a webResource of site type and associate it with the current audit
     *
     * @param url
     */
    protected void createEmptySiteResource(String url) {
        Site site = webResourceDataService.createSite(url);
        getAudit().setSubject(site);
        webResourceDataService.saveOrUpdate(site);
    }

    /**
     * Create the main webResource attached to the audit and then
     * passed to the scenario loader service
     *
     * @return a Site instance
     */
    private WebResource createWebResource() {
        WebResource webResource;
        if (scope.equals(PAGE)) {
            webResource = webResourceDataService.createPage(this.targetUrl);
        } else {
            webResource = webResourceDataService.createSite(this.targetUrl);
        }
        webResource.setAudit(getAudit());
        webResourceDataService.saveOrUpdate(webResource);
        getAudit().setSubject(webResource);
        return webResource;
    }

}
