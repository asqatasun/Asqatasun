UPDATE `TEST`
SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`, 'v5/en/business-rules/rgaa-v412/', 'v6/Business-rules/RGAA-v4.1.2/')
WHERE Cd_Test LIKE 'Rgaa412-%';

UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`, '/rule--', '/Rule-') WHERE Cd_Test LIKE 'Rgaa412-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'01.images/','01.Images/') WHERE Cd_Test LIKE 'Rgaa412-1-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'02.frames/','02.Frames/') WHERE Cd_Test LIKE 'Rgaa412-2-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'03.colours/','03.Colours/') WHERE Cd_Test LIKE 'Rgaa412-3-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'04.multimedia/','04.Multimedia/') WHERE Cd_Test LIKE 'Rgaa412-4-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'05.tables/','05.Tables/') WHERE Cd_Test LIKE 'Rgaa412-5-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'06.links/','06.Links/') WHERE Cd_Test LIKE 'Rgaa412-6-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'07.scripts/','07.Scripts/') WHERE Cd_Test LIKE 'Rgaa412-7-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'08.mandatory_elements/','08.Mandatory_elements/') WHERE Cd_Test LIKE 'Rgaa412-8-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'09.structure_of_information/','09.Structure_of_information/') WHERE Cd_Test LIKE 'Rgaa412-9-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'10.presentation_of_information/','10.Presentation_of_information/') WHERE Cd_Test LIKE 'Rgaa412-10-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'11.forms/','11.Forms/') WHERE Cd_Test LIKE 'Rgaa412-11-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'12.navigation/','12.Navigation/') WHERE Cd_Test LIKE 'Rgaa412-12-%';
UPDATE `TEST` SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`,'13.consultation/','13.Consultation/') WHERE Cd_Test LIKE 'Rgaa412-13-%';

UPDATE `TEST`
SET `Rule_Design_Url` = REPLACE(`Rule_Design_Url`, 'v5/en/Business-rules/RGAA-v4/', 'v6/Business-rules/RGAA-v4/')
WHERE Cd_Test LIKE 'Rgaa40-%';
