UPDATE test
SET rule_design_url = REPLACE(rule_design_url, 'v5/en/business-rules/rgaa-v412/', 'v6/Business-rules/RGAA-v4.1.2/')
WHERE cd_Test LIKE 'Rgaa412-%';

UPDATE test SET rule_design_url = REPLACE(rule_design_url, '/rule--', '/Rule-') WHERE cd_Test LIKE 'Rgaa412-%';

UPDATE test
SET rule_design_url = REPLACE(rule_design_url, 'v5/en/Business-rules/RGAA-v4/', 'v6/Business-rules/RGAA-v4/')
WHERE cd_Test LIKE 'Rgaa40-%';
