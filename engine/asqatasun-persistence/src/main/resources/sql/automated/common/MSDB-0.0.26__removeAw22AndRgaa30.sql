DELETE act, aa, a FROM AUDIT as a
        LEFT JOIN AUDIT_PARAMETER as ap on (a.Id_Audit=ap.Id_Audit)
        LEFT JOIN ACT_AUDIT as aa on (a.Id_Audit=aa.AUDIT_Id_Audit)
        LEFT JOIN ACT as act on (act.Id_Act=aa.ACT_Id_Act)
        LEFT JOIN PARAMETER as p on (p.Id_Parameter=ap.Id_Parameter) where p.Parameter_Value like '%Aw22%';
DELETE act, aa, a FROM AUDIT as a
    LEFT JOIN AUDIT_PARAMETER as ap on (a.Id_Audit=ap.Id_Audit)
    LEFT JOIN ACT_AUDIT as aa on (a.Id_Audit=aa.AUDIT_Id_Audit)
    LEFT JOIN ACT as act on (act.Id_Act=aa.ACT_Id_Act)
    LEFT JOIN PARAMETER as p on (p.Id_Parameter=ap.Id_Parameter) where p.Parameter_Value like '%Rgaa22%';
DELETE act, aa, a FROM AUDIT as a
    LEFT JOIN AUDIT_PARAMETER as ap on (a.Id_Audit=ap.Id_Audit)
    LEFT JOIN ACT_AUDIT as aa on (a.Id_Audit=aa.AUDIT_Id_Audit)
    LEFT JOIN ACT as act on (act.Id_Act=aa.ACT_Id_Act)
    LEFT JOIN PARAMETER as p on (p.Id_Parameter=ap.Id_Parameter) where p.Parameter_Value like '%Rgaa30%';
DELETE FROM TEST WHERE cd_test like '%Aw22%';
DELETE FROM TEST WHERE cd_test like '%Rgaa22%';
DELETE FROM TEST WHERE cd_test like '%Rgaa30%';
DELETE FROM CRITERION WHERE cd_criterion like '%Aw22%';
DELETE FROM CRITERION WHERE cd_criterion like '%Rgaa22%';
DELETE FROM CRITERION WHERE cd_criterion like '%Rgaa30%';
DELETE FROM THEME WHERE cd_theme like '%Aw22%';
DELETE FROM THEME WHERE cd_theme like '%Rgaa22%';
DELETE FROM THEME WHERE cd_theme like '%Rgaa30%';
DELETE FROM REFERENCE where cd_reference like '%Aw22%';
DELETE FROM REFERENCE where cd_reference like '%Rgaa22%';
DELETE FROM REFERENCE where cd_reference like '%Rgaa30%';
DELETE cr FROM CONTRACT_REFERENTIAL as cr left join REFERENTIAL as r on (r.Id_Referential=cr.REFERENTIAL_Id_Referential) where r.code like '%Aw22%';
DELETE cr FROM CONTRACT_REFERENTIAL as cr left join REFERENTIAL as r on (r.Id_Referential=cr.REFERENTIAL_Id_Referential) where r.code like '%Rgaa22%';
DELETE cr FROM CONTRACT_REFERENTIAL as cr left join REFERENTIAL as r on (r.Id_Referential=cr.REFERENTIAL_Id_Referential) where r.code like '%Rgaa30%';
DELETE FROM REFERENTIAL where code like '%Aw22%';
DELETE FROM REFERENTIAL where code like '%Rgaa22%';
DELETE FROM REFERENTIAL where code like '%Rgaa30%';
UPDATE PARAMETER SET is_default=true where parameter_value='Rgaa40;LEVEL_2';
