SET DATABASE SQL SYNTAX MYS TRUE;

create table AUDIT
(
    Id_Audit                 bigint auto_increment
        primary key,
    Comment                  varchar(255) null,
    Dt_Creation              datetime     null,
    Status                   varchar(255) null
);

create table AUDIT_SCOPE
(
    Id_Scope bigint auto_increment
        primary key,
    Code     varchar(255) default 'PAGE' not null,
    Label    varchar(255) default 'Page' not null,
    constraint UNIQUE_INDEX_TGSI_SCOPE_Code
        unique (Code)
) charset = utf8mb3;

create table DECISION_LEVEL
(
    Id_Decision_Level bigint auto_increment
        primary key,
    Cd_Decision_Level varchar(255) null,
    Description       varchar(255) null,
    Label             varchar(255) not null
);

create table EVIDENCE
(
    Id_Evidence bigint auto_increment
        primary key,
    Cd_Evidence varchar(255) null,
    Description varchar(255) null,
    Long_Label  varchar(255) null
);

create table FUNCTIONALITY
(
    Id_Functionality bigint auto_increment
        primary key,
    Code             varchar(45)   not null,
    Label            varchar(255)  null,
    Description      varchar(2048) null,
    constraint UNIQUE_INDEX_TGSI_FUNCTIONALITY_Code
        unique (Code)
) charset = utf8mb3;

create table LEVEL
(
    Id_Level    bigint auto_increment
        primary key,
    Cd_Level    varchar(255) null,
    Description varchar(255) null,
    Label       varchar(255) null,
    Rank        int          null
);

create table NOMENCLATURE
(
    Id_Nomenclature        bigint auto_increment
        primary key,
    Cd_Nomenclature        varchar(255) null,
    Description            varchar(255) null,
    Long_Label             varchar(255) null,
    Short_Label            varchar(255) null,
    Id_Nomenclature_Parent bigint       null,
    constraint FKBF856B7795431825
        foreign key (Id_Nomenclature_Parent) references NOMENCLATURE (Id_Nomenclature)
);

create table NOMENCLATURE_ELEMENT
(
    DTYPE                   varchar(31)  not null,
    Id_Nomenclature_Element bigint auto_increment
        primary key,
    Label                   varchar(255) not null,
    shortValue              int          null,
    Id_Nomenclature         bigint       null,
    constraint FK44F856145FAB5EF2
        foreign key (Id_Nomenclature) references NOMENCLATURE (Id_Nomenclature)
);

create table OPTION_FAMILY
(
    Id_Option_Family bigint auto_increment
        primary key,
    Code             varchar(45)   not null,
    Label            varchar(255)  null,
    Description      varchar(2048) null,
    constraint UNIQUE_INDEX_TGSI_OPTION_FAMILY_Code
        unique (Code)
) charset = utf8mb3;

create table OPTION_VALUE
(
    Id_Option                      bigint auto_increment
        primary key,
    Code                           varchar(45)      not null,
    Label                          varchar(255)     null,
    Description                    varchar(2048)    null,
    Is_Restriction                 bit default b'1' null,
    OPTION_FAMILY_Id_Option_Family bigint           not null,
    constraint UNIQUE_INDEX_TGSI_OPTION_Code
        unique (Code),
    constraint FK_TGSI_OPTION_TGSI_OPTION_FAMILY
        foreign key (OPTION_FAMILY_Id_Option_Family) references OPTION_FAMILY (Id_Option_Family)
) charset = utf8mb3;

create table OPTION_ELEMENT
(
    Id_Option_Element bigint auto_increment
        primary key,
    OPTION_Id_Option  bigint       not null,
    Value             varchar(255) null,
    constraint INDEX_UNIQUE_PAIR_OPTION_ELEMENT
        unique (OPTION_Id_Option, Value),
    constraint FK_TGSI_OPTION_ELEMENT_TGSI_OPTION
        foreign key (OPTION_Id_Option) references OPTION_VALUE (Id_Option)
) charset = utf8mb3;

create table PARAMETER_FAMILY
(
    Id_Parameter_Family bigint auto_increment
        primary key,
    Cd_Parameter_Family varchar(255) not null,
    Description         varchar(255) null,
    Long_Label          varchar(255) null,
    Short_Label         varchar(255) null,
    constraint Cd_Parameter_Family_UNIQUE
        unique (Cd_Parameter_Family)
);

create table PARAMETER_ELEMENT
(
    Id_Parameter_Element bigint auto_increment
        primary key,
    Cd_Parameter_Element varchar(255) not null,
    Id_Parameter_Family  bigint       not null,
    Short_Label          varchar(255) null,
    Long_Label           varchar(255) null,
    constraint Cd_Parameter_Element_UNIQUE
        unique (Cd_Parameter_Element),
    constraint fk_PARAMETER_ELEMENT_PARAMETER
        foreign key (Id_Parameter_Family) references PARAMETER_FAMILY (Id_Parameter_Family)
);

create table PARAMETER
(
    Id_Parameter         bigint auto_increment
        primary key,
    Parameter_Value      varchar(1000)    not null,
    Id_Parameter_Element bigint           not null,
    Is_Default           bit default b'0' null,
    constraint Unique_Param_Element_Type_Param_value
        unique (Parameter_Value, Id_Parameter_Element),
    constraint fk_PARAMETER_PARAMETER_ELEMENT
        foreign key (Id_Parameter_Element) references PARAMETER_ELEMENT (Id_Parameter_Element)
);

create table AUDIT_PARAMETER
(
    Id_Audit     bigint not null,
    Id_Parameter bigint not null,
    constraint fk_AUDIT_PARAMETER_AUDIT
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint fk_AUDIT_PARAMETER_PARAMETER
        foreign key (Id_Parameter) references PARAMETER (Id_Parameter)
            on delete cascade
);

create table REFERENCE
(
    Id_Reference     bigint auto_increment
        primary key,
    Cd_Reference     varchar(255)     null,
    Description      varchar(255)     null,
    Label            varchar(255)     not null,
    Rank             int              null,
    Url              varchar(255)     null,
    Id_Default_Level bigint default 2 null,
    constraint Cd_Reference_UNIQUE
        unique (Cd_Reference),
    constraint fk_Ref_Level
        foreign key (Id_Default_Level) references LEVEL (Id_Level)
);

create table REFERENTIAL
(
    Id_Referential bigint auto_increment
        primary key,
    Code           varchar(45)   not null,
    Label          varchar(255)  null,
    Description    varchar(2048) null,
    constraint UNIQUE_INDEX_TGSI_REFERENTIAL_Code
        unique (Code)
) charset = utf8mb3;

create table ROLE
(
    Id_Role      bigint auto_increment
        primary key,
    Role_Name    varchar(255) not null,
    ROLE_Id_Role bigint       null,
    constraint FK_TGSI_ROLE_TGSI_ROLE
        foreign key (ROLE_Id_Role) references ROLE (Id_Role)
            on update cascade on delete cascade
) charset = utf8mb3;

create index INDEX_TGSI_ROLE_ROLE_Id_Role
    on ROLE (ROLE_Id_Role);

create table SCOPE
(
    Id_Scope    bigint auto_increment
        primary key,
    Code        varchar(255) null,
    Description varchar(255) null,
    Label       varchar(255) not null
);

create table STANDARD_MESSAGE
(
    Id_Standard_Message bigint auto_increment
        primary key,
    Cd_Standard_Message varchar(255) null,
    Label               varchar(255) null,
    Text                varchar(255) null
);

create table TAG
(
    Id_Tag bigint auto_increment
        primary key,
    Value  varchar(255) null,
    constraint Value_UNIQUE
        unique (Value)
);

create table AUDIT_TAG
(
    Id_Audit bigint not null,
    Id_Tag   bigint not null,
    constraint FK838E6E96ABC9863D
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK838E6E96F94B364A
        foreign key (Id_Tag) references TAG (Id_Tag)
);

create table THEME
(
    Id_Theme    bigint auto_increment
        primary key,
    Cd_Theme    varchar(255) null,
    Description varchar(255) null,
    Label       varchar(255) not null,
    Rank        int          null,
    constraint Cd_Theme_UNIQUE
        unique (Cd_Theme)
);

create table CRITERION
(
    Id_Criterion           bigint auto_increment
        primary key,
    Cd_Criterion           varchar(255) null,
    Description            varchar(255) null,
    Label                  varchar(255) null,
    Rank                   int          null,
    Url                    varchar(255) null,
    Reference_Id_Reference bigint       null,
    Theme_Id_Theme         bigint       null,
    constraint Cd_Criterion_UNIQUE
        unique (Cd_Criterion),
    constraint FKBCFA1E81D03CE506
        foreign key (Reference_Id_Reference) references REFERENCE (Id_Reference),
    constraint FKBCFA1E81E8F67244
        foreign key (Theme_Id_Theme) references THEME (Id_Theme)
);

create table TEST
(
    Id_Test           bigint auto_increment
        primary key,
    Cd_Test           varchar(255)               null,
    Description       varchar(255)               null,
    Label             varchar(255)               null,
    Rank              int                        null,
    Weight            decimal(2, 1) default 1.0  null,
    Rule_Archive_Name varchar(255)               null,
    Rule_Class_Name   varchar(255)               null,
    Rule_Design_Url   varchar(255)               null,
    Id_Criterion      bigint                     null,
    Id_Decision_Level bigint                     null,
    Id_Level          bigint                     null,
    Id_Rule           bigint                     null,
    Id_Scope          bigint                     null,
    No_Process        bit           default b'1' null,
    constraint Cd_Test_UNIQUE
        unique (Cd_Test),
    constraint FK273C9250C99824
        foreign key (Id_Scope) references SCOPE (Id_Scope),
    constraint FK273C926CCA4C3E
        foreign key (Id_Criterion) references CRITERION (Id_Criterion),
    constraint FK273C9272343A84
        foreign key (Id_Level) references LEVEL (Id_Level),
    constraint FK273C92CCA757AD
        foreign key (Id_Decision_Level) references DECISION_LEVEL (Id_Decision_Level)
);

create table AUDIT_TEST
(
    Id_Audit bigint not null,
    Id_Test  bigint not null,
    constraint FK838E6E96493EC9C2
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK838E6E96A17A5FA8
        foreign key (Id_Test) references TEST (Id_Test)
);

create index Cd_Theme_Index
    on THEME (Cd_Theme);

create table USERS
(
    Id_User      bigint auto_increment
        primary key,
    Email1       varchar(255)              not null,
    Password     varchar(255)              not null,
    Name         varchar(255) default ''   not null,
    First_Name   varchar(255) default ''   not null,
    Address      varchar(255)              null,
    Phone_Number varchar(255)              null,
    Email2       varchar(255)              null,
    Web1         varchar(2048)             null,
    Web2         varchar(2048)             null,
    Identica_Id  varchar(255)              null,
    Twitter_Id   varchar(255)              null,
    ROLE_Id_Role bigint                    not null,
    Activated    bit          default b'1' null,
    constraint UNIQUE_INDEX_TGSI_USER_Email1
        unique (Email1),
    constraint FK_TGSI_USER_TGSI_ROLE
        foreign key (ROLE_Id_Role) references ROLE (Id_Role)
) charset = utf8mb3;

create table CONTRACT
(
    Id_Contract  bigint auto_increment
        primary key,
    Label        varchar(255) not null,
    Begin_Date   datetime     not null,
    End_Date     datetime     not null,
    Renewal_Date datetime     null,
    Price        float        null,
    USER_Id_User bigint       not null,
    constraint FK_TGSI_CONTRACT_TGSI_USER
        foreign key (USER_Id_User) references USERS (Id_User)
            on delete cascade
) charset = utf8mb3;

create table ACT
(
    Id_Act               bigint auto_increment
        primary key,
    Begin_Date           datetime                      not null,
    End_Date             datetime                      null,
    Status               varchar(255)                  not null,
    CONTRACT_Id_Contract bigint                        not null,
    SCOPE_Id_Scope       bigint                        not null,
    Client_Ip            varchar(46) default '0.0.0.0' not null,
    constraint FK_TGSI_ACT_TGSI_CONTRACT
        foreign key (CONTRACT_Id_Contract) references CONTRACT (Id_Contract)
            on delete cascade,
    constraint FK_TGSI_ACT_TGSI_SCOPE
        foreign key (SCOPE_Id_Scope) references AUDIT_SCOPE (Id_Scope)
) charset = utf8mb3;

create index INDEX_TGSI_ACT_CONTRACT_Id_Contract
    on ACT (CONTRACT_Id_Contract);

create index INDEX_TGSI_ACT_SCOPE_Id_Scope
    on ACT (SCOPE_Id_Scope);

create table ACT_AUDIT
(
    ACT_Id_Act     bigint not null,
    AUDIT_Id_Audit bigint not null,
    constraint INDEX_UNIQUE_PAIR_ACT_AUDIT
        unique (ACT_Id_Act, AUDIT_Id_Audit),
    constraint FK_TGSI_ACT_AUDIT_AUDIT
        foreign key (AUDIT_Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK_TGSI_ACT_AUDIT_TGSI_ACT
        foreign key (ACT_Id_Act) references ACT (Id_Act)
            on delete cascade
) charset = utf8mb3;

create index INDEX_TGSI_ACT_AUDIT_ACT_Id_Act
    on ACT_AUDIT (ACT_Id_Act);

create index INDEX_TGSI_ACT_AUDIT_AUDIT_Id_Audit
    on ACT_AUDIT (AUDIT_Id_Audit);

create table CONTRACT_FUNCTIONALITY
(
    CONTRACT_Id_Contract           bigint not null,
    FUNCTIONALITY_Id_Functionality bigint not null,
    constraint INDEX_UNIQUE_PAIR_CONTRACT_FUNCTIONALITY
        unique (FUNCTIONALITY_Id_Functionality, CONTRACT_Id_Contract),
    constraint FK_TGSI_CONTRACT_FUNCTIONALITY_TGSI_CONTRACT
        foreign key (CONTRACT_Id_Contract) references CONTRACT (Id_Contract)
            on update cascade on delete cascade,
    constraint FK_TGSI_CONTRACT_FUNCTIONALITY_TGSI_FUNCTIONALITY
        foreign key (FUNCTIONALITY_Id_Functionality) references FUNCTIONALITY (Id_Functionality)
) charset = utf8mb3;

create index INDEX_TGSI_CONTRACT_FUNCTIONALITY_CONTRACT_Id_Contract
    on CONTRACT_FUNCTIONALITY (CONTRACT_Id_Contract);

create index INDEX_TGSI_CONTRACT_FUNCTIONALITY_FUNCTIONALITY_Id_Functionality
    on CONTRACT_FUNCTIONALITY (FUNCTIONALITY_Id_Functionality);

create table CONTRACT_OPTION_ELEMENT
(
    OPTION_ELEMENT_Id_Option_Element bigint not null,
    CONTRACT_Id_Contract             bigint not null,
    constraint INDEX_UNIQUE_PAIR_CONTRACT_OPTION_ELEMENT
        unique (OPTION_ELEMENT_Id_Option_Element, CONTRACT_Id_Contract),
    constraint FK_TGSI_CONTRACT_OPTION_ELEMENT_TGSI_CONTRACT
        foreign key (CONTRACT_Id_Contract) references CONTRACT (Id_Contract)
            on update cascade on delete cascade,
    constraint FK_TGSI_CONTRACT_OPTION_ELEMENT_TGSI_OPTION_ELEMENT
        foreign key (OPTION_ELEMENT_Id_Option_Element) references OPTION_ELEMENT (Id_Option_Element)
            on update cascade on delete cascade
) charset = utf8mb3;

create table CONTRACT_REFERENTIAL
(
    CONTRACT_Id_Contract       bigint not null,
    REFERENTIAL_Id_Referential bigint not null,
    constraint INDEX_UNIQUE_PAIR_CONTRACT_REFERENTIAL
        unique (REFERENTIAL_Id_Referential, CONTRACT_Id_Contract),
    constraint FK_TGSI_CONTRACT_REFERENTIAL_TGSI_CONTRACT
        foreign key (CONTRACT_Id_Contract) references CONTRACT (Id_Contract)
            on update cascade on delete cascade,
    constraint FK_TGSI_CONTRACT_REFERENTIAL_TGSI_REFERENTIAL
        foreign key (REFERENTIAL_Id_Referential) references REFERENTIAL (Id_Referential)
) charset = utf8mb3;

create index INDEX_TGSI_CONTRACT_REFERENTIAL_CONTRACT_Id_Contract
    on CONTRACT_REFERENTIAL (CONTRACT_Id_Contract);

create index INDEX_TGSI_CONTRACT_REFERENTIAL_REFERENTIAL_Id_Referential
    on CONTRACT_REFERENTIAL (REFERENTIAL_Id_Referential);

create table SCENARIO
(
    Id_Scenario          bigint auto_increment
        primary key,
    Date_Of_Creation     datetime     not null,
    Label                varchar(255) not null,
    Content              mediumtext   not null,
    Description          varchar(255) null,
    CONTRACT_Id_Contract bigint       not null,
    constraint FK_TGSI_SCENARIO_TGSI_CONTRACT
        foreign key (CONTRACT_Id_Contract) references CONTRACT (Id_Contract)
            on update cascade on delete cascade
) charset = utf8mb3;

create index INDEX_TGSI_SCENARIO_CONTRACT_Id_Contract
    on SCENARIO (CONTRACT_Id_Contract);

create index INDEX_TGSI_USER_ROLE_Id_Role
    on USERS (ROLE_Id_Role);

create table USER_OPTION_ELEMENT
(
    OPTION_ELEMENT_Id_Option_Element bigint not null,
    USER_Id_User                     bigint not null,
    constraint INDEX_UNIQUE_PAIR_USER_OPTION_ELEMENT
        unique (OPTION_ELEMENT_Id_Option_Element, USER_Id_User),
    constraint FK_TGSI_USER_OPTION_ELEMENT_TGSI_OPTION_ELEMENT
        foreign key (OPTION_ELEMENT_Id_Option_Element) references OPTION_ELEMENT (Id_Option_Element)
            on update cascade on delete cascade,
    constraint FK_TGSI_USER_OPTION_ELEMENT_TGSI_USER
        foreign key (USER_Id_User) references USERS (Id_User)
            on update cascade on delete cascade
) charset = utf8mb3;

create table WEB_RESOURCE
(
    DTYPE                  varchar(31)   not null,
    Id_Web_Resource        bigint auto_increment
        primary key,
    Label                  varchar(255)  null,
    Url                    varchar(2048) not null,
    Rank                   int default 0 null,
    Id_Audit               bigint        null,
    Id_Web_Resource_Parent bigint        null,
    constraint FKD9A970B92F70FF12
        foreign key (Id_Web_Resource_Parent) references WEB_RESOURCE (Id_Web_Resource)
            on update cascade on delete cascade,
    constraint FKD9A970B9493EC9C2
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on update cascade on delete cascade
);

create table CONTENT
(
    DTYPE            varchar(31)   not null,
    Id_Content       bigint auto_increment
        primary key,
    Dt_Loading       datetime      null,
    Http_Status_Code int           not null,
    Uri              varchar(2048) not null,
    Binary_Content   mediumblob    null,
    Adapted_Content  longtext      null,
    Source           longtext      null,
    Charset          varchar(255)  null,
    Doctype          varchar(512)  null,
    Id_Audit         bigint        null,
    Id_Page          bigint        null,
    constraint FK6382C059493EC9C2
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK6382C059A8A177A1
        foreign key (Id_Page) references WEB_RESOURCE (Id_Web_Resource)
            on delete cascade
);

create index DTYPE_Index_CONTENT
    on CONTENT (DTYPE);

create index Http_Status_Code_Index
    on CONTENT (Http_Status_Code);

create index Uri_Index
    on CONTENT (Uri);

create table CONTENT_RELATIONSHIP
(
    Id_Content_Child  bigint not null,
    Id_Content_Parent bigint not null,
    primary key (Id_Content_Child, Id_Content_Parent),
    constraint FKBA33205E620A8494
        foreign key (Id_Content_Parent) references CONTENT (Id_Content)
            on delete cascade,
    constraint FKBA33205EBA71C750
        foreign key (Id_Content_Child) references CONTENT (Id_Content)
            on delete cascade
);

create table PRE_PROCESS_RESULT
(
    Id_Pre_Process_Result bigint auto_increment
        primary key,
    Pre_Process_Key       varchar(255) not null,
    Pre_Process_Value     mediumtext   null,
    Id_Audit              bigint       not null,
    Id_Web_Resource       bigint       null,
    constraint Key_Wr_Audit
        unique (Pre_Process_Key, Id_Web_Resource, Id_Audit),
    constraint fk_PRE_PROCESS_RESULT_AUDIT
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint fk_PRE_PROCESS_RESULT_WEB_RESOURCE
        foreign key (Id_Web_Resource) references WEB_RESOURCE (Id_Web_Resource)
            on delete cascade
);

create table PROCESS_RESULT
(
    DTYPE                    varchar(31)  not null,
    Id_Process_Result        bigint auto_increment
        primary key,
    Element_Counter          int          null,
    Definite_Value           varchar(255) null,
    Indefinite_Value         mediumtext   null,
    Id_Audit_Gross_Result    bigint       null,
    Id_Audit_Net_Result      bigint       null,
    Id_Process_Result_Parent bigint       null,
    Id_Web_Resource          bigint       not null,
    Id_Test                  bigint       null,
    constraint Id_Test
        unique (Id_Test, Id_Web_Resource, Id_Audit_Gross_Result),
    constraint Id_Test_2
        unique (Id_Test, Id_Web_Resource, Id_Audit_Net_Result),
    constraint FK1C41A80D2E48600
        foreign key (Id_Web_Resource) references WEB_RESOURCE (Id_Web_Resource)
            on delete cascade,
    constraint FK1C41A80D8146180B
        foreign key (Id_Audit_Gross_Result) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK1C41A80DA17A5FA8
        foreign key (Id_Test) references TEST (Id_Test),
    constraint FK1C41A80DB6D0E092
        foreign key (Id_Audit_Net_Result) references AUDIT (Id_Audit)
            on delete cascade,
    constraint FK1C41A80DFA349234
        foreign key (Id_Process_Result_Parent) references PROCESS_RESULT (Id_Process_Result)
);

create table PROCESS_REMARK
(
    DTYPE                varchar(31)   not null,
    Id_Process_Remark    bigint auto_increment
        primary key,
    Issue                varchar(255)  null,
    Message_Code         varchar(255)  null,
    Selected_Element     varchar(255)  null,
    Selection_Expression varchar(255)  null,
    Character_Position   int           null,
    Line_Number          int           null,
    Target               varchar(5000) null,
    Snippet              mediumtext    null,
    Id_Process_Result    bigint        null,
    constraint FK1C3EA37045A988AD
        foreign key (Id_Process_Result) references PROCESS_RESULT (Id_Process_Result)
            on delete cascade
);

create table EVIDENCE_ELEMENT
(
    Id_Evidence_Element              bigint auto_increment
        primary key,
    Element_Value                    mediumtext not null,
    EVIDENCE_Id_Evidence             bigint     null,
    PROCESS_REMARK_Id_Process_Remark bigint     null,
    constraint FK698B98F425AD22C4
        foreign key (PROCESS_REMARK_Id_Process_Remark) references PROCESS_REMARK (Id_Process_Remark)
            on delete cascade,
    constraint FK698B98F4C94A0CBA
        foreign key (EVIDENCE_Id_Evidence) references EVIDENCE (Id_Evidence)
);

create index Issue_Index
    on PROCESS_REMARK (Issue);

create index Definite_Value_Index
    on PROCESS_RESULT (Definite_Value);

create index DTYPE_Index_WEB_RESOURCE
    on WEB_RESOURCE (DTYPE);

create index Url_Index
    on WEB_RESOURCE (Url);

create table WEB_RESOURCE_STATISTICS
(
    Id_Web_Resource_Statistics bigint auto_increment
        primary key,
    Mark                       float          null,
    Raw_Mark                   float          null,
    Nb_Passed                  int            null,
    Nb_Failed                  int            null,
    Nb_Nmi                     int            null,
    Nb_Na                      int            null,
    Nb_Suspected               int            null,
    Nb_Detected                int            null,
    Nb_Not_Tested              int            null,
    Weighted_Passed            decimal(10, 1) null,
    Weighted_Failed            decimal(10, 1) null,
    Weighted_Nmi               decimal(10, 1) null,
    Weighted_Na                decimal(10, 1) null,
    Nb_Failed_Occurrences      int            null,
    Nb_Invalid_Test            int            null,
    Id_Audit                   bigint         null,
    Id_Web_Resource            bigint         null,
    Http_Status_Code           int default -1 null,
    constraint fk_WEB_RESOURCE_STATISTICS_AUDIT
        foreign key (Id_Audit) references AUDIT (Id_Audit)
            on delete cascade,
    constraint fk_WEB_RESOURCE_STATISTICS_WEB_RESOURCE
        foreign key (Id_Web_Resource) references WEB_RESOURCE (Id_Web_Resource)
            on delete cascade
);

create table CRITERION_STATISTICS
(
    Id_Criterion_Statistics    bigint auto_increment
        primary key,
    Nb_Passed                  int          null,
    Nb_Failed                  int          null,
    Nb_Nmi                     int          null,
    Nb_Na                      int          null,
    Nb_Suspected               int          null,
    Nb_Detected                int          null,
    Nb_Not_Tested              int          null,
    Criterion_Result           varchar(255) null,
    Id_Criterion               bigint       null,
    Id_Web_Resource_Statistics bigint       null,
    constraint fk_CRITERION_STATISTICS_CRITERION
        foreign key (Id_Criterion) references CRITERION (Id_Criterion)
            on delete cascade,
    constraint fk_CRITERION_STATISTICS_WEB_RESOURCE_STATISTICS
        foreign key (Id_Web_Resource_Statistics) references WEB_RESOURCE_STATISTICS (Id_Web_Resource_Statistics)
            on delete cascade
);

create table TEST_STATISTICS
(
    Id_Test_Statistics         bigint auto_increment
        primary key,
    Nb_Passed                  int    null,
    Nb_Failed                  int    null,
    Nb_Nmi                     int    null,
    Nb_Na                      int    null,
    Nb_Suspected               int    null,
    Nb_Detected                int    null,
    Nb_Not_Tested              int    null,
    Id_Test                    bigint null,
    Id_Web_Resource_Statistics bigint null,
    constraint fk_TEST_STATISTICS_TEST
        foreign key (Id_Test) references TEST (Id_Test)
            on delete cascade,
    constraint fk_TEST_STATISTICS_WEB_RESOURCE_STATISTICS
        foreign key (Id_Web_Resource_Statistics) references WEB_RESOURCE_STATISTICS (Id_Web_Resource_Statistics)
            on delete cascade
);

create index fk_THEME_STATISTICS_THEME
    on TEST_STATISTICS (Id_Test);

create index fk_THEME_STATISTICS_WEB_RESOURCE_STATISTICS
    on TEST_STATISTICS (Id_Web_Resource_Statistics);

create table THEME_STATISTICS
(
    Id_Theme_Statistics        bigint auto_increment
        primary key,
    Nb_Passed                  int    null,
    Nb_Failed                  int    null,
    Nb_Nmi                     int    null,
    Nb_Na                      int    null,
    Nb_Suspected               int    null,
    Nb_Detected                int    null,
    Nb_Not_Tested              int    null,
    Id_Theme                   bigint null,
    Id_Web_Resource_Statistics bigint null,
    constraint fk_THEME_STATISTICS_THEME
        foreign key (Id_Theme) references THEME (Id_Theme)
            on delete cascade,
    constraint fk_THEME_STATISTICS_WEB_RESOURCE_STATISTICS
        foreign key (Id_Web_Resource_Statistics) references WEB_RESOURCE_STATISTICS (Id_Web_Resource_Statistics)
            on delete cascade
);
