--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Debian 12.9-1.pgdg110+1)
-- Dumped by pg_dump version 12.16 (Ubuntu 12.16-0ubuntu0.20.04.1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

SET default_tablespace = '';
SET default_table_access_method = heap;

--
-- Name: act; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE act (
    id_act bigint NOT NULL,
    begin_date timestamp with time zone NOT NULL,
    end_date timestamp with time zone,
    status character varying(255) NOT NULL,
    contract_id_contract bigint NOT NULL,
    scope_id_scope bigint NOT NULL,
    client_ip character varying(46) DEFAULT '0.0.0.0'::character varying NOT NULL
);

--
-- Name: act_audit; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE act_audit (
    act_id_act bigint NOT NULL,
    audit_id_audit bigint NOT NULL
);

--
-- Name: act_id_act_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE act_id_act_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: act_id_act_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE act_id_act_seq OWNED BY act.id_act;


--
-- Name: audit; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE audit (
    id_audit bigint NOT NULL,
    comment character varying(255) DEFAULT NULL::character varying,
    dt_creation timestamp with time zone,
    status character varying(255) DEFAULT NULL::character varying
);

--
-- Name: audit_id_audit_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE audit_id_audit_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: audit_id_audit_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE audit_id_audit_seq OWNED BY audit.id_audit;


--
-- Name: audit_parameter; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE audit_parameter (
    id_audit bigint NOT NULL,
    id_parameter bigint NOT NULL
);

--
-- Name: audit_scope; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE audit_scope (
    id_scope bigint NOT NULL,
    code character varying(255) DEFAULT 'PAGE'::character varying NOT NULL,
    label character varying(255) DEFAULT 'Page'::character varying NOT NULL
);

--
-- Name: audit_scope_id_scope_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE audit_scope_id_scope_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: audit_scope_id_scope_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE audit_scope_id_scope_seq OWNED BY audit_scope.id_scope;


--
-- Name: audit_tag; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE audit_tag (
    id_audit bigint NOT NULL,
    id_tag bigint NOT NULL
);

--
-- Name: audit_test; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE audit_test (
    id_audit bigint NOT NULL,
    id_test bigint NOT NULL
);

--
-- Name: content; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE content (
    dtype character varying(31) NOT NULL,
    id_content bigint NOT NULL,
    dt_loading timestamp with time zone,
    http_status_code bigint NOT NULL,
    uri character varying(2048) NOT NULL,
    binary_content bytea,
    adapted_content text,
    source text,
    charset character varying(255) DEFAULT NULL::character varying,
    doctype character varying(512) DEFAULT NULL::character varying,
    id_audit bigint,
    id_page bigint
);

--
-- Name: content_id_content_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE content_id_content_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: content_id_content_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE content_id_content_seq OWNED BY content.id_content;


--
-- Name: content_relationship; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE content_relationship (
    id_content_child bigint NOT NULL,
    id_content_parent bigint NOT NULL
);

--
-- Name: contract; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE contract (
    id_contract bigint NOT NULL,
    label character varying(255) NOT NULL,
    begin_date timestamp with time zone NOT NULL,
    end_date timestamp with time zone NOT NULL,
    renewal_date timestamp with time zone,
    price double precision,
    user_id_user bigint NOT NULL
);

--
-- Name: contract_functionality; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE contract_functionality (
    contract_id_contract bigint NOT NULL,
    functionality_id_functionality bigint NOT NULL
);

--
-- Name: contract_id_contract_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE contract_id_contract_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contract_id_contract_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE contract_id_contract_seq OWNED BY contract.id_contract;


--
-- Name: contract_option_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE contract_option_element (
    option_element_id_option_element bigint NOT NULL,
    contract_id_contract bigint NOT NULL
);


--
-- Name: contract_referential; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE contract_referential (
    contract_id_contract bigint NOT NULL,
    referential_id_referential bigint NOT NULL
);


--
-- Name: criterion; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE criterion (
    id_criterion bigint NOT NULL,
    cd_criterion character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) DEFAULT NULL::character varying,
    rank bigint,
    url character varying(255) DEFAULT NULL::character varying,
    reference_id_reference bigint,
    theme_id_theme bigint
);

--
-- Name: criterion_id_criterion_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE criterion_id_criterion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: criterion_id_criterion_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE criterion_id_criterion_seq OWNED BY criterion.id_criterion;


--
-- Name: criterion_statistics; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE criterion_statistics (
    id_criterion_statistics bigint NOT NULL,
    nb_passed bigint,
    nb_failed bigint,
    nb_nmi bigint,
    nb_na bigint,
    nb_suspected bigint,
    nb_detected bigint,
    nb_not_tested bigint,
    criterion_result character varying(255) DEFAULT NULL::character varying,
    id_criterion bigint,
    id_web_resource_statistics bigint
);


--
-- Name: criterion_statistics_id_criterion_statistics_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE criterion_statistics_id_criterion_statistics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: criterion_statistics_id_criterion_statistics_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE criterion_statistics_id_criterion_statistics_seq OWNED BY criterion_statistics.id_criterion_statistics;


--
-- Name: decision_level; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE decision_level (
    id_decision_level bigint NOT NULL,
    cd_decision_level character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) NOT NULL
);

--
-- Name: decision_level_id_decision_level_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE decision_level_id_decision_level_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: decision_level_id_decision_level_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE decision_level_id_decision_level_seq OWNED BY decision_level.id_decision_level;


--
-- Name: evidence; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE evidence (
    id_evidence bigint NOT NULL,
    cd_evidence character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    long_label character varying(255) DEFAULT NULL::character varying
);

--
-- Name: evidence_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE evidence_element (
    id_evidence_element bigint NOT NULL,
    element_value text NOT NULL,
    evidence_id_evidence bigint,
    process_remark_id_process_remark bigint
);

--
-- Name: evidence_element_id_evidence_element_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE evidence_element_id_evidence_element_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: evidence_element_id_evidence_element_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE evidence_element_id_evidence_element_seq OWNED BY evidence_element.id_evidence_element;


--
-- Name: evidence_id_evidence_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE evidence_id_evidence_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: evidence_id_evidence_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE evidence_id_evidence_seq OWNED BY evidence.id_evidence;


--
-- Name: functionality; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE functionality (
    id_functionality bigint NOT NULL,
    code character varying(45) NOT NULL,
    label character varying(255) DEFAULT NULL::character varying,
    description character varying(2048) DEFAULT NULL::character varying
);

--
-- Name: functionality_id_functionality_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE functionality_id_functionality_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: functionality_id_functionality_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE functionality_id_functionality_seq OWNED BY functionality.id_functionality;


--
-- Name: level; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE level (
    id_level bigint NOT NULL,
    cd_level character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) DEFAULT NULL::character varying,
    rank bigint
);

--
-- Name: level_id_level_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE level_id_level_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: level_id_level_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE level_id_level_seq OWNED BY level.id_level;


--
-- Name: nomenclature; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE nomenclature (
    id_nomenclature bigint NOT NULL,
    cd_nomenclature character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    long_label character varying(255) DEFAULT NULL::character varying,
    short_label character varying(255) DEFAULT NULL::character varying,
    id_nomenclature_parent bigint
);

--
-- Name: nomenclature_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE nomenclature_element (
    dtype character varying(31) NOT NULL,
    id_nomenclature_element bigint NOT NULL,
    label character varying(255) NOT NULL,
    shortvalue bigint,
    id_nomenclature bigint
);

--
-- Name: nomenclature_element_id_nomenclature_element_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE nomenclature_element_id_nomenclature_element_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: nomenclature_element_id_nomenclature_element_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE nomenclature_element_id_nomenclature_element_seq OWNED BY nomenclature_element.id_nomenclature_element;


--
-- Name: nomenclature_id_nomenclature_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE nomenclature_id_nomenclature_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: nomenclature_id_nomenclature_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE nomenclature_id_nomenclature_seq OWNED BY nomenclature.id_nomenclature;


--
-- Name: option_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE option_element (
    id_option_element bigint NOT NULL,
    option_id_option bigint NOT NULL,
    value character varying(255) DEFAULT NULL::character varying
);

--
-- Name: option_element_id_option_element_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE option_element_id_option_element_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: option_element_id_option_element_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE option_element_id_option_element_seq OWNED BY option_element.id_option_element;


--
-- Name: option_family; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE option_family (
    id_option_family bigint NOT NULL,
    code character varying(45) NOT NULL,
    label character varying(255) DEFAULT NULL::character varying,
    description character varying(2048) DEFAULT NULL::character varying
);

--
-- Name: option_family_id_option_family_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE option_family_id_option_family_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: option_family_id_option_family_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE option_family_id_option_family_seq OWNED BY option_family.id_option_family;


--
-- Name: option_value; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE option_value (
    id_option bigint NOT NULL,
    code character varying(45) NOT NULL,
    label character varying(255) DEFAULT NULL::character varying,
    description character varying(2048) DEFAULT NULL::character varying,
    is_restriction boolean,
    option_family_id_option_family bigint NOT NULL
);

--
-- Name: option_value_id_option_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE option_value_id_option_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: option_value_id_option_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE option_value_id_option_seq OWNED BY option_value.id_option;


--
-- Name: parameter; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE parameter (
    id_parameter bigint NOT NULL,
    parameter_value character varying(1000) NOT NULL,
    id_parameter_element bigint NOT NULL,
    is_default boolean
);

--
-- Name: parameter_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE parameter_element (
    id_parameter_element bigint NOT NULL,
    cd_parameter_element character varying(255) NOT NULL,
    id_parameter_family bigint NOT NULL,
    short_label character varying(255) DEFAULT NULL::character varying,
    long_label character varying(255) DEFAULT NULL::character varying
);

--
-- Name: parameter_element_id_parameter_element_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE parameter_element_id_parameter_element_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: parameter_element_id_parameter_element_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE parameter_element_id_parameter_element_seq OWNED BY parameter_element.id_parameter_element;


--
-- Name: parameter_family; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE parameter_family (
    id_parameter_family bigint NOT NULL,
    cd_parameter_family character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    long_label character varying(255) DEFAULT NULL::character varying,
    short_label character varying(255) DEFAULT NULL::character varying
);

--
-- Name: parameter_family_id_parameter_family_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE parameter_family_id_parameter_family_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: parameter_family_id_parameter_family_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE parameter_family_id_parameter_family_seq OWNED BY parameter_family.id_parameter_family;


--
-- Name: parameter_id_parameter_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE parameter_id_parameter_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: parameter_id_parameter_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE parameter_id_parameter_seq OWNED BY parameter.id_parameter;


--
-- Name: pre_process_result; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE pre_process_result (
    id_pre_process_result bigint NOT NULL,
    pre_process_key character varying(255) NOT NULL,
    pre_process_value text,
    id_audit bigint NOT NULL,
    id_web_resource bigint
);

--
-- Name: pre_process_result_id_pre_process_result_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE pre_process_result_id_pre_process_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: pre_process_result_id_pre_process_result_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE pre_process_result_id_pre_process_result_seq OWNED BY pre_process_result.id_pre_process_result;


--
-- Name: process_remark; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE process_remark (
    dtype character varying(31) NOT NULL,
    id_process_remark bigint NOT NULL,
    issue character varying(255) DEFAULT NULL::character varying,
    message_code character varying(255) DEFAULT NULL::character varying,
    selected_element character varying(255) DEFAULT NULL::character varying,
    selection_expression character varying(255) DEFAULT NULL::character varying,
    character_position bigint,
    line_number bigint,
    target character varying(5000) DEFAULT NULL::character varying,
    snippet text,
    id_process_result bigint
);

--
-- Name: process_remark_id_process_remark_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE process_remark_id_process_remark_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: process_remark_id_process_remark_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE process_remark_id_process_remark_seq OWNED BY process_remark.id_process_remark;


--
-- Name: process_result; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE process_result (
    dtype character varying(31) NOT NULL,
    id_process_result bigint NOT NULL,
    element_counter bigint,
    definite_value character varying(255) DEFAULT NULL::character varying,
    indefinite_value text,
    id_audit_gross_result bigint,
    id_audit_net_result bigint,
    id_process_result_parent bigint,
    id_web_resource bigint NOT NULL,
    id_test bigint
);

--
-- Name: process_result_aud; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE process_result_aud (
    dtype character varying(31) NOT NULL,
    id_process_result bigint NOT NULL,
    rev bigint NOT NULL,
    revtype smallint,
    element_counter bigint,
    id_process_result_parent bigint,
    definite_value character varying(255) DEFAULT NULL::character varying,
    manual_audit_comment character varying(255) DEFAULT NULL::character varying,
    manual_definite_value character varying(255) DEFAULT NULL::character varying
);

--
-- Name: process_result_id_process_result_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE process_result_id_process_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: process_result_id_process_result_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE process_result_id_process_result_seq OWNED BY process_result.id_process_result;


--
-- Name: reference; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE reference (
    id_reference bigint NOT NULL,
    cd_reference character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) NOT NULL,
    rank bigint,
    url character varying(255) DEFAULT NULL::character varying,
    id_default_level bigint DEFAULT '2'::bigint
);

--
-- Name: reference_id_reference_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE reference_id_reference_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: reference_id_reference_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE reference_id_reference_seq OWNED BY reference.id_reference;


--
-- Name: referential; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE referential (
    id_referential bigint NOT NULL,
    code character varying(45) NOT NULL,
    label character varying(255) DEFAULT NULL::character varying,
    description character varying(2048) DEFAULT NULL::character varying
);

--
-- Name: referential_id_referential_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE referential_id_referential_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: referential_id_referential_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE referential_id_referential_seq OWNED BY referential.id_referential;


--
-- Name: revinfo; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE revinfo (
    rev bigint NOT NULL,
    revtstmp bigint
);

--
-- Name: revinfo_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE revinfo_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: revinfo_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE revinfo_seq OWNED BY revinfo.rev;


--
-- Name: role; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE role (
    id_role bigint NOT NULL,
    role_name character varying(255) NOT NULL,
    role_id_role bigint
);

--
-- Name: role_id_role_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE role_id_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: role_id_role_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE role_id_role_seq OWNED BY role.id_role;


--
-- Name: scenario; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE scenario (
    id_scenario bigint NOT NULL,
    date_of_creation timestamp with time zone NOT NULL,
    label character varying(255) NOT NULL,
    content text NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    contract_id_contract bigint NOT NULL
);

--
-- Name: scenario_id_scenario_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE scenario_id_scenario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: scenario_id_scenario_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE scenario_id_scenario_seq OWNED BY scenario.id_scenario;


--
-- Name: scope; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE scope (
    id_scope bigint NOT NULL,
    code character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) NOT NULL
);

--
-- Name: scope_id_scope_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE scope_id_scope_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: scope_id_scope_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE scope_id_scope_seq OWNED BY scope.id_scope;


--
-- Name: standard_message; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE standard_message (
    id_standard_message bigint NOT NULL,
    cd_standard_message character varying(255) DEFAULT NULL::character varying,
    label character varying(255) DEFAULT NULL::character varying,
    text character varying(255) DEFAULT NULL::character varying
);

--
-- Name: standard_message_id_standard_message_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE standard_message_id_standard_message_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: standard_message_id_standard_message_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE standard_message_id_standard_message_seq OWNED BY standard_message.id_standard_message;


--
-- Name: tag; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE tag (
    id_tag bigint NOT NULL,
    value character varying(255) DEFAULT NULL::character varying
);

--
-- Name: tag_id_tag_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE tag_id_tag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: tag_id_tag_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE tag_id_tag_seq OWNED BY tag.id_tag;


--
-- Name: test; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE test (
    id_test bigint NOT NULL,
    cd_test character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) DEFAULT NULL::character varying,
    rank bigint,
    weight numeric(2,1) DEFAULT 1.0,
    rule_archive_name character varying(255) DEFAULT NULL::character varying,
    rule_class_name character varying(255) DEFAULT NULL::character varying,
    rule_design_url character varying(255) DEFAULT NULL::character varying,
    id_criterion bigint,
    id_decision_level bigint,
    id_level bigint,
    id_rule bigint,
    id_scope bigint,
    no_process boolean
);

--
-- Name: test_id_test_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE test_id_test_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: test_id_test_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE test_id_test_seq OWNED BY test.id_test;


--
-- Name: test_statistics; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE test_statistics (
    id_test_statistics bigint NOT NULL,
    nb_passed bigint,
    nb_failed bigint,
    nb_nmi bigint,
    nb_na bigint,
    nb_suspected bigint,
    nb_detected bigint,
    nb_not_tested bigint,
    id_test bigint,
    id_web_resource_statistics bigint
);

--
-- Name: test_statistics_id_test_statistics_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE test_statistics_id_test_statistics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: test_statistics_id_test_statistics_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE test_statistics_id_test_statistics_seq OWNED BY test_statistics.id_test_statistics;


--
-- Name: theme; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE theme (
    id_theme bigint NOT NULL,
    cd_theme character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    label character varying(255) NOT NULL,
    rank bigint
);

--
-- Name: theme_id_theme_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE theme_id_theme_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: theme_id_theme_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE theme_id_theme_seq OWNED BY theme.id_theme;


--
-- Name: theme_statistics; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE theme_statistics (
    id_theme_statistics bigint NOT NULL,
    nb_passed bigint,
    nb_failed bigint,
    nb_nmi bigint,
    nb_na bigint,
    nb_suspected bigint,
    nb_detected bigint,
    nb_not_tested bigint,
    id_theme bigint,
    id_web_resource_statistics bigint
);

--
-- Name: theme_statistics_id_theme_statistics_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE theme_statistics_id_theme_statistics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: theme_statistics_id_theme_statistics_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE theme_statistics_id_theme_statistics_seq OWNED BY theme_statistics.id_theme_statistics;


--
-- Name: user; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE "user" (
    id_user bigint NOT NULL,
    email1 character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    first_name character varying(255) DEFAULT ''::character varying NOT NULL,
    address character varying(255) DEFAULT NULL::character varying,
    phone_number character varying(255) DEFAULT NULL::character varying,
    email2 character varying(255) DEFAULT NULL::character varying,
    web1 character varying(2048) DEFAULT NULL::character varying,
    web2 character varying(2048) DEFAULT NULL::character varying,
    identica_id character varying(255) DEFAULT NULL::character varying,
    twitter_id character varying(255) DEFAULT NULL::character varying,
    role_id_role bigint NOT NULL,
    activated boolean
);

--
-- Name: user_id_user_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE user_id_user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: user_id_user_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE user_id_user_seq OWNED BY "user".id_user;


--
-- Name: user_option_element; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE user_option_element (
    option_element_id_option_element bigint NOT NULL,
    user_id_user bigint NOT NULL
);

--
-- Name: web_resource; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE web_resource (
    dtype character varying(31) NOT NULL,
    id_web_resource bigint NOT NULL,
    label character varying(255) DEFAULT NULL::character varying,
    url character varying(2048) NOT NULL,
    rank bigint DEFAULT '0'::bigint,
    id_audit bigint,
    id_web_resource_parent bigint
);

--
-- Name: web_resource_id_web_resource_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE web_resource_id_web_resource_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: web_resource_id_web_resource_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE web_resource_id_web_resource_seq OWNED BY web_resource.id_web_resource;


--
-- Name: web_resource_statistics; Type: TABLE; Schema: asqatasun;
--

CREATE TABLE web_resource_statistics (
    id_web_resource_statistics bigint NOT NULL,
    mark double precision,
    raw_mark double precision,
    nb_passed bigint,
    nb_failed bigint,
    nb_nmi bigint,
    nb_na bigint,
    nb_suspected bigint,
    nb_detected bigint,
    nb_not_tested bigint,
    weighted_passed numeric(10,1) DEFAULT NULL::numeric,
    weighted_failed numeric(10,1) DEFAULT NULL::numeric,
    weighted_nmi numeric(10,1) DEFAULT NULL::numeric,
    weighted_na numeric(10,1) DEFAULT NULL::numeric,
    nb_failed_occurrences bigint,
    nb_invalid_test bigint,
    id_audit bigint,
    id_web_resource bigint,
    http_status_code bigint DEFAULT '-1'::bigint
);

--
-- Name: web_resource_statistics_id_web_resource_statistics_seq; Type: SEQUENCE; Schema: asqatasun;
--

CREATE SEQUENCE web_resource_statistics_id_web_resource_statistics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: web_resource_statistics_id_web_resource_statistics_seq; Type: SEQUENCE OWNED BY; Schema: asqatasun;
--

ALTER SEQUENCE web_resource_statistics_id_web_resource_statistics_seq OWNED BY web_resource_statistics.id_web_resource_statistics;


--
-- Name: act id_act; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY act ALTER COLUMN id_act SET DEFAULT nextval('act_id_act_seq'::regclass);


--
-- Name: audit id_audit; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY audit ALTER COLUMN id_audit SET DEFAULT nextval('audit_id_audit_seq'::regclass);


--
-- Name: audit_scope id_scope; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_scope ALTER COLUMN id_scope SET DEFAULT nextval('audit_scope_id_scope_seq'::regclass);


--
-- Name: content id_content; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY content ALTER COLUMN id_content SET DEFAULT nextval('content_id_content_seq'::regclass);


--
-- Name: contract id_contract; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY contract ALTER COLUMN id_contract SET DEFAULT nextval('contract_id_contract_seq'::regclass);


--
-- Name: criterion id_criterion; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion ALTER COLUMN id_criterion SET DEFAULT nextval('criterion_id_criterion_seq'::regclass);


--
-- Name: criterion_statistics id_criterion_statistics; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion_statistics ALTER COLUMN id_criterion_statistics SET DEFAULT nextval('criterion_statistics_id_criterion_statistics_seq'::regclass);


--
-- Name: decision_level id_decision_level; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY decision_level ALTER COLUMN id_decision_level SET DEFAULT nextval('decision_level_id_decision_level_seq'::regclass);


--
-- Name: evidence id_evidence; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence ALTER COLUMN id_evidence SET DEFAULT nextval('evidence_id_evidence_seq'::regclass);


--
-- Name: evidence_element id_evidence_element; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence_element ALTER COLUMN id_evidence_element SET DEFAULT nextval('evidence_element_id_evidence_element_seq'::regclass);


--
-- Name: functionality id_functionality; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY functionality ALTER COLUMN id_functionality SET DEFAULT nextval('functionality_id_functionality_seq'::regclass);


--
-- Name: level id_level; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY level ALTER COLUMN id_level SET DEFAULT nextval('level_id_level_seq'::regclass);


--
-- Name: nomenclature id_nomenclature; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature ALTER COLUMN id_nomenclature SET DEFAULT nextval('nomenclature_id_nomenclature_seq'::regclass);


--
-- Name: nomenclature_element id_nomenclature_element; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature_element ALTER COLUMN id_nomenclature_element SET DEFAULT nextval('nomenclature_element_id_nomenclature_element_seq'::regclass);


--
-- Name: option_element id_option_element; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY option_element ALTER COLUMN id_option_element SET DEFAULT nextval('option_element_id_option_element_seq'::regclass);


--
-- Name: option_family id_option_family; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY option_family ALTER COLUMN id_option_family SET DEFAULT nextval('option_family_id_option_family_seq'::regclass);


--
-- Name: option_value id_option; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY option_value ALTER COLUMN id_option SET DEFAULT nextval('option_value_id_option_seq'::regclass);


--
-- Name: parameter id_parameter; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter ALTER COLUMN id_parameter SET DEFAULT nextval('parameter_id_parameter_seq'::regclass);


--
-- Name: parameter_element id_parameter_element; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter_element ALTER COLUMN id_parameter_element SET DEFAULT nextval('parameter_element_id_parameter_element_seq'::regclass);


--
-- Name: parameter_family id_parameter_family; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter_family ALTER COLUMN id_parameter_family SET DEFAULT nextval('parameter_family_id_parameter_family_seq'::regclass);


--
-- Name: pre_process_result id_pre_process_result; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY pre_process_result ALTER COLUMN id_pre_process_result SET DEFAULT nextval('pre_process_result_id_pre_process_result_seq'::regclass);


--
-- Name: process_remark id_process_remark; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY process_remark ALTER COLUMN id_process_remark SET DEFAULT nextval('process_remark_id_process_remark_seq'::regclass);


--
-- Name: process_result id_process_result; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result ALTER COLUMN id_process_result SET DEFAULT nextval('process_result_id_process_result_seq'::regclass);


--
-- Name: reference id_reference; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY reference ALTER COLUMN id_reference SET DEFAULT nextval('reference_id_reference_seq'::regclass);


--
-- Name: referential id_referential; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY referential ALTER COLUMN id_referential SET DEFAULT nextval('referential_id_referential_seq'::regclass);


--
-- Name: revinfo rev; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY revinfo ALTER COLUMN rev SET DEFAULT nextval('revinfo_seq'::regclass);


--
-- Name: role id_role; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY role ALTER COLUMN id_role SET DEFAULT nextval('role_id_role_seq'::regclass);


--
-- Name: scenario id_scenario; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY scenario ALTER COLUMN id_scenario SET DEFAULT nextval('scenario_id_scenario_seq'::regclass);


--
-- Name: scope id_scope; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY scope ALTER COLUMN id_scope SET DEFAULT nextval('scope_id_scope_seq'::regclass);


--
-- Name: standard_message id_standard_message; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY standard_message ALTER COLUMN id_standard_message SET DEFAULT nextval('standard_message_id_standard_message_seq'::regclass);


--
-- Name: tag id_tag; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY tag ALTER COLUMN id_tag SET DEFAULT nextval('tag_id_tag_seq'::regclass);


--
-- Name: test id_test; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY test ALTER COLUMN id_test SET DEFAULT nextval('test_id_test_seq'::regclass);


--
-- Name: test_statistics id_test_statistics; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY test_statistics ALTER COLUMN id_test_statistics SET DEFAULT nextval('test_statistics_id_test_statistics_seq'::regclass);


--
-- Name: theme id_theme; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY theme ALTER COLUMN id_theme SET DEFAULT nextval('theme_id_theme_seq'::regclass);


--
-- Name: theme_statistics id_theme_statistics; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY theme_statistics ALTER COLUMN id_theme_statistics SET DEFAULT nextval('theme_statistics_id_theme_statistics_seq'::regclass);


--
-- Name: user id_user; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY "user" ALTER COLUMN id_user SET DEFAULT nextval('user_id_user_seq'::regclass);


--
-- Name: web_resource id_web_resource; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource ALTER COLUMN id_web_resource SET DEFAULT nextval('web_resource_id_web_resource_seq'::regclass);


--
-- Name: web_resource_statistics id_web_resource_statistics; Type: DEFAULT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource_statistics ALTER COLUMN id_web_resource_statistics SET DEFAULT nextval('web_resource_statistics_id_web_resource_statistics_seq'::regclass);

--
-- Data for Name: user_option_element; Type: TABLE DATA; Schema: asqatasun;
--



--
-- Data for Name: web_resource; Type: TABLE DATA; Schema: asqatasun;
--



--
-- Data for Name: web_resource_statistics; Type: TABLE DATA; Schema: asqatasun;
--



--
-- Name: act_id_act_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('act_id_act_seq', 1, true);


--
-- Name: audit_id_audit_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('audit_id_audit_seq', 1, true);


--
-- Name: audit_scope_id_scope_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('audit_scope_id_scope_seq', 6, true);


--
-- Name: content_id_content_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('content_id_content_seq', 1, true);


--
-- Name: contract_id_contract_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('contract_id_contract_seq', 1, true);


--
-- Name: criterion_id_criterion_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('criterion_id_criterion_seq', 1, true);


--
-- Name: criterion_statistics_id_criterion_statistics_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('criterion_statistics_id_criterion_statistics_seq', 1, true);


--
-- Name: decision_level_id_decision_level_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('decision_level_id_decision_level_seq', 1, true);


--
-- Name: evidence_element_id_evidence_element_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('evidence_element_id_evidence_element_seq', 1, true);


--
-- Name: evidence_id_evidence_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('evidence_id_evidence_seq', 1, true);


--
-- Name: functionality_id_functionality_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('functionality_id_functionality_seq', 1, true);


--
-- Name: level_id_level_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('level_id_level_seq', 1, true);


--
-- Name: nomenclature_element_id_nomenclature_element_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('nomenclature_element_id_nomenclature_element_seq', 1, true);


--
-- Name: nomenclature_id_nomenclature_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('nomenclature_id_nomenclature_seq', 1, true);


--
-- Name: option_element_id_option_element_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('option_element_id_option_element_seq', 1, true);


--
-- Name: option_family_id_option_family_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('option_family_id_option_family_seq', 1, true);


--
-- Name: option_value_id_option_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('option_value_id_option_seq', 1, true);


--
-- Name: parameter_element_id_parameter_element_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('parameter_element_id_parameter_element_seq', 1, true);


--
-- Name: parameter_family_id_parameter_family_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('parameter_family_id_parameter_family_seq', 1, true);


--
-- Name: parameter_id_parameter_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('parameter_id_parameter_seq', 1, true);


--
-- Name: pre_process_result_id_pre_process_result_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('pre_process_result_id_pre_process_result_seq', 1, true);


--
-- Name: process_remark_id_process_remark_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('process_remark_id_process_remark_seq', 1, true);


--
-- Name: process_result_id_process_result_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('process_result_id_process_result_seq', 1, true);


--
-- Name: reference_id_reference_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('reference_id_reference_seq', 1, true);


--
-- Name: referential_id_referential_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('referential_id_referential_seq', 1, true);


--
-- Name: revinfo_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('revinfo_seq', 1, true);


--
-- Name: role_id_role_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('role_id_role_seq', 1, true);


--
-- Name: scenario_id_scenario_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('scenario_id_scenario_seq', 1, true);


--
-- Name: scope_id_scope_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('scope_id_scope_seq', 1, true);


--
-- Name: standard_message_id_standard_message_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('standard_message_id_standard_message_seq', 1, true);


--
-- Name: tag_id_tag_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('tag_id_tag_seq', 1, true);


--
-- Name: test_id_test_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('test_id_test_seq', 1, true);


--
-- Name: test_statistics_id_test_statistics_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('test_statistics_id_test_statistics_seq', 1, true);


--
-- Name: theme_id_theme_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('theme_id_theme_seq', 1, true);


--
-- Name: theme_statistics_id_theme_statistics_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('theme_statistics_id_theme_statistics_seq', 1, true);


--
-- Name: user_id_user_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('user_id_user_seq', 1, true);


--
-- Name: web_resource_id_web_resource_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('web_resource_id_web_resource_seq', 1, true);


--
-- Name: web_resource_statistics_id_web_resource_statistics_seq; Type: SEQUENCE SET; Schema: asqatasun;
--

SELECT pg_catalog.setval('web_resource_statistics_id_web_resource_statistics_seq', 1, true);


--
-- Name: act idx_16389_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY act
    ADD CONSTRAINT idx_16389_primary PRIMARY KEY (id_act);


--
-- Name: audit idx_16399_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit
    ADD CONSTRAINT idx_16399_primary PRIMARY KEY (id_audit);


--
-- Name: audit_scope idx_16413_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_scope
    ADD CONSTRAINT idx_16413_primary PRIMARY KEY (id_scope);


--
-- Name: content idx_16430_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content
    ADD CONSTRAINT idx_16430_primary PRIMARY KEY (id_content);


--
-- Name: content_relationship idx_16439_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content_relationship
    ADD CONSTRAINT idx_16439_primary PRIMARY KEY (id_content_child, id_content_parent);


--
-- Name: contract idx_16444_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract
    ADD CONSTRAINT idx_16444_primary PRIMARY KEY (id_contract);


--
-- Name: criterion idx_16459_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion
    ADD CONSTRAINT idx_16459_primary PRIMARY KEY (id_criterion);


--
-- Name: criterion_statistics idx_16472_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion_statistics
    ADD CONSTRAINT idx_16472_primary PRIMARY KEY (id_criterion_statistics);


--
-- Name: decision_level idx_16479_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY decision_level
    ADD CONSTRAINT idx_16479_primary PRIMARY KEY (id_decision_level);


--
-- Name: evidence idx_16490_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence
    ADD CONSTRAINT idx_16490_primary PRIMARY KEY (id_evidence);


--
-- Name: evidence_element idx_16502_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence_element
    ADD CONSTRAINT idx_16502_primary PRIMARY KEY (id_evidence_element);


--
-- Name: functionality idx_16519_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY functionality
    ADD CONSTRAINT idx_16519_primary PRIMARY KEY (id_functionality);


--
-- Name: level idx_16530_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY level
    ADD CONSTRAINT idx_16530_primary PRIMARY KEY (id_level);


--
-- Name: nomenclature idx_16542_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature
    ADD CONSTRAINT idx_16542_primary PRIMARY KEY (id_nomenclature);


--
-- Name: nomenclature_element idx_16555_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature_element
    ADD CONSTRAINT idx_16555_primary PRIMARY KEY (id_nomenclature_element);


--
-- Name: option_element idx_16561_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY option_element
    ADD CONSTRAINT idx_16561_primary PRIMARY KEY (id_option_element);


--
-- Name: option_family idx_16568_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY option_family
    ADD CONSTRAINT idx_16568_primary PRIMARY KEY (id_option_family);


--
-- Name: option_value idx_16579_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY option_value
    ADD CONSTRAINT idx_16579_primary PRIMARY KEY (id_option);


--
-- Name: parameter idx_16590_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT idx_16590_primary PRIMARY KEY (id_parameter);


--
-- Name: parameter_element idx_16599_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter_element
    ADD CONSTRAINT idx_16599_primary PRIMARY KEY (id_parameter_element);


--
-- Name: parameter_family idx_16610_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter_family
    ADD CONSTRAINT idx_16610_primary PRIMARY KEY (id_parameter_family);


--
-- Name: pre_process_result idx_16622_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY pre_process_result
    ADD CONSTRAINT idx_16622_primary PRIMARY KEY (id_pre_process_result);


--
-- Name: process_remark idx_16631_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_remark
    ADD CONSTRAINT idx_16631_primary PRIMARY KEY (id_process_remark);


--
-- Name: process_result idx_16645_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT idx_16645_primary PRIMARY KEY (id_process_result);


--
-- Name: process_result_aud idx_16655_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result_aud
    ADD CONSTRAINT idx_16655_primary PRIMARY KEY (id_process_result, rev);


--
-- Name: reference idx_16666_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY reference
    ADD CONSTRAINT idx_16666_primary PRIMARY KEY (id_reference);


--
-- Name: referential idx_16679_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY referential
    ADD CONSTRAINT idx_16679_primary PRIMARY KEY (id_referential);


--
-- Name: revinfo idx_16690_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY revinfo
    ADD CONSTRAINT idx_16690_primary PRIMARY KEY (rev);


--
-- Name: role idx_16696_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY role
    ADD CONSTRAINT idx_16696_primary PRIMARY KEY (id_role);


--
-- Name: scenario idx_16702_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT idx_16702_primary PRIMARY KEY (id_scenario);


--
-- Name: scope idx_16712_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY scope
    ADD CONSTRAINT idx_16712_primary PRIMARY KEY (id_scope);


--
-- Name: standard_message idx_16723_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY standard_message
    ADD CONSTRAINT idx_16723_primary PRIMARY KEY (id_standard_message);


--
-- Name: tag idx_16735_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT idx_16735_primary PRIMARY KEY (id_tag);


--
-- Name: test idx_16742_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test
    ADD CONSTRAINT idx_16742_primary PRIMARY KEY (id_test);


--
-- Name: test_statistics idx_16758_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test_statistics
    ADD CONSTRAINT idx_16758_primary PRIMARY KEY (id_test_statistics);


--
-- Name: theme idx_16764_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY theme
    ADD CONSTRAINT idx_16764_primary PRIMARY KEY (id_theme);


--
-- Name: theme_statistics idx_16775_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY theme_statistics
    ADD CONSTRAINT idx_16775_primary PRIMARY KEY (id_theme_statistics);


--
-- Name: user idx_16781_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT idx_16781_primary PRIMARY KEY (id_user);


--
-- Name: web_resource idx_16802_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource
    ADD CONSTRAINT idx_16802_primary PRIMARY KEY (id_web_resource);


--
-- Name: web_resource_statistics idx_16813_primary; Type: CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource_statistics
    ADD CONSTRAINT idx_16813_primary PRIMARY KEY (id_web_resource_statistics);


--
-- Name: idx_16389_index_tgsi_act_contract_id_contract; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16389_index_tgsi_act_contract_id_contract ON act USING btree (contract_id_contract);


--
-- Name: idx_16389_index_tgsi_act_scope_id_scope; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16389_index_tgsi_act_scope_id_scope ON act USING btree (scope_id_scope);


--
-- Name: idx_16394_index_tgsi_act_audit_act_id_act; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16394_index_tgsi_act_audit_act_id_act ON act_audit USING btree (act_id_act);


--
-- Name: idx_16394_index_tgsi_act_audit_audit_id_audit; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16394_index_tgsi_act_audit_audit_id_audit ON act_audit USING btree (audit_id_audit);


--
-- Name: idx_16394_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16394_index_unique_pair ON act_audit USING btree (act_id_act, audit_id_audit);


--
-- Name: idx_16408_fk_audit_parameter_audit; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16408_fk_audit_parameter_audit ON audit_parameter USING btree (id_audit);


--
-- Name: idx_16408_fk_audit_parameter_parameter; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16408_fk_audit_parameter_parameter ON audit_parameter USING btree (id_parameter);


--
-- Name: idx_16413_unique_index_tgsi_scope_code; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16413_unique_index_tgsi_scope_code ON audit_scope USING btree (code);


--
-- Name: idx_16422_fk838e6e96abc9863d; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16422_fk838e6e96abc9863d ON audit_tag USING btree (id_audit);


--
-- Name: idx_16422_fk838e6e96f94b364a; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16422_fk838e6e96f94b364a ON audit_tag USING btree (id_tag);


--
-- Name: idx_16425_fk838e6e96493ec9c2; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16425_fk838e6e96493ec9c2 ON audit_test USING btree (id_audit);


--
-- Name: idx_16425_fk838e6e96a17a5fa8; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16425_fk838e6e96a17a5fa8 ON audit_test USING btree (id_test);


--
-- Name: idx_16430_dtype_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16430_dtype_index ON content USING btree (dtype);


--
-- Name: idx_16430_fk6382c059493ec9c2; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16430_fk6382c059493ec9c2 ON content USING btree (id_audit);


--
-- Name: idx_16430_fk6382c059a8a177a1; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16430_fk6382c059a8a177a1 ON content USING btree (id_page);


--
-- Name: idx_16430_http_status_code_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16430_http_status_code_index ON content USING btree (http_status_code);


--
-- Name: idx_16430_uri_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16430_uri_index ON content USING btree (uri);


--
-- Name: idx_16439_fkba33205e620a8494; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16439_fkba33205e620a8494 ON content_relationship USING btree (id_content_parent);


--
-- Name: idx_16439_fkba33205eba71c750; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16439_fkba33205eba71c750 ON content_relationship USING btree (id_content_child);


--
-- Name: idx_16444_fk_tgsi_contract_tgsi_user; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16444_fk_tgsi_contract_tgsi_user ON contract USING btree (user_id_user);


--
-- Name: idx_16448_index_tgsi_contract_functionality_contract_id_contrac; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16448_index_tgsi_contract_functionality_contract_id_contrac ON contract_functionality USING btree (contract_id_contract);


--
-- Name: idx_16448_index_tgsi_contract_functionality_functionality_id_fu; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16448_index_tgsi_contract_functionality_functionality_id_fu ON contract_functionality USING btree (functionality_id_functionality);


--
-- Name: idx_16448_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16448_index_unique_pair ON contract_functionality USING btree (functionality_id_functionality, contract_id_contract);


--
-- Name: idx_16451_fk_tgsi_contract_option_element_tgsi_contract; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16451_fk_tgsi_contract_option_element_tgsi_contract ON contract_option_element USING btree (contract_id_contract);


--
-- Name: idx_16451_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16451_index_unique_pair ON contract_option_element USING btree (option_element_id_option_element, contract_id_contract);


--
-- Name: idx_16454_index_tgsi_contract_referential_contract_id_contract; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16454_index_tgsi_contract_referential_contract_id_contract ON contract_referential USING btree (contract_id_contract);


--
-- Name: idx_16454_index_tgsi_contract_referential_referential_id_refere; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16454_index_tgsi_contract_referential_referential_id_refere ON contract_referential USING btree (referential_id_referential);


--
-- Name: idx_16454_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16454_index_unique_pair ON contract_referential USING btree (referential_id_referential, contract_id_contract);


--
-- Name: idx_16459_cd_criterion_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16459_cd_criterion_unique ON criterion USING btree (cd_criterion);


--
-- Name: idx_16459_fkbcfa1e81d03ce506; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16459_fkbcfa1e81d03ce506 ON criterion USING btree (reference_id_reference);


--
-- Name: idx_16459_fkbcfa1e81e8f67244; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16459_fkbcfa1e81e8f67244 ON criterion USING btree (theme_id_theme);


--
-- Name: idx_16472_fk_criterion_statistics_criterion; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16472_fk_criterion_statistics_criterion ON criterion_statistics USING btree (id_criterion);


--
-- Name: idx_16472_fk_criterion_statistics_web_resource_statistics; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16472_fk_criterion_statistics_web_resource_statistics ON criterion_statistics USING btree (id_web_resource_statistics);

--
-- Name: idx_16502_fk698b98f425ad22c4; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16502_fk698b98f425ad22c4 ON evidence_element USING btree (process_remark_id_process_remark);

--
-- Name: idx_16502_fk698b98f4c94a0cba; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16502_fk698b98f4c94a0cba ON evidence_element USING btree (evidence_id_evidence);

--
-- Name: idx_16519_unique_index_tgsi_functionality_code; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16519_unique_index_tgsi_functionality_code ON functionality USING btree (code);

--
-- Name: idx_16542_fkbf856b7795431825; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16542_fkbf856b7795431825 ON nomenclature USING btree (id_nomenclature_parent);

--
-- Name: idx_16555_fk44f856145fab5ef2; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16555_fk44f856145fab5ef2 ON nomenclature_element USING btree (id_nomenclature);

--
-- Name: idx_16561_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16561_index_unique_pair ON option_element USING btree (option_id_option, value);


--
-- Name: idx_16568_unique_index_tgsi_option_family_code; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16568_unique_index_tgsi_option_family_code ON option_family USING btree (code);


--
-- Name: idx_16579_fk_tgsi_option_tgsi_option_family; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16579_fk_tgsi_option_tgsi_option_family ON option_value USING btree (option_family_id_option_family);


--
-- Name: idx_16579_unique_index_tgsi_option_code; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16579_unique_index_tgsi_option_code ON option_value USING btree (code);


--
-- Name: idx_16590_fk_parameter_parameter_element; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16590_fk_parameter_parameter_element ON parameter USING btree (id_parameter_element);


--
-- Name: idx_16590_unique_param_element_type_param_value; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16590_unique_param_element_type_param_value ON parameter USING btree (parameter_value, id_parameter_element);


--
-- Name: idx_16599_cd_parameter_element_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16599_cd_parameter_element_unique ON parameter_element USING btree (cd_parameter_element);


--
-- Name: idx_16599_fk_parameter_element_parameter; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16599_fk_parameter_element_parameter ON parameter_element USING btree (id_parameter_family);


--
-- Name: idx_16610_cd_parameter_family_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16610_cd_parameter_family_unique ON parameter_family USING btree (cd_parameter_family);


--
-- Name: idx_16622_fk_pre_process_result_audit; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16622_fk_pre_process_result_audit ON pre_process_result USING btree (id_audit);


--
-- Name: idx_16622_fk_pre_process_result_web_resource; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16622_fk_pre_process_result_web_resource ON pre_process_result USING btree (id_web_resource);


--
-- Name: idx_16622_key_wr_audit; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16622_key_wr_audit ON pre_process_result USING btree (pre_process_key, id_web_resource, id_audit);


--
-- Name: idx_16631_fk1c3ea37045a988ad; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16631_fk1c3ea37045a988ad ON process_remark USING btree (id_process_result);


--
-- Name: idx_16631_issue_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16631_issue_index ON process_remark USING btree (issue);


--
-- Name: idx_16645_definite_value_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_definite_value_index ON process_result USING btree (definite_value);


--
-- Name: idx_16645_fk1c41a80d2e48600; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_fk1c41a80d2e48600 ON process_result USING btree (id_web_resource);


--
-- Name: idx_16645_fk1c41a80d8146180b; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_fk1c41a80d8146180b ON process_result USING btree (id_audit_gross_result);


--
-- Name: idx_16645_fk1c41a80da17a5fa8; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_fk1c41a80da17a5fa8 ON process_result USING btree (id_test);


--
-- Name: idx_16645_fk1c41a80db6d0e092; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_fk1c41a80db6d0e092 ON process_result USING btree (id_audit_net_result);


--
-- Name: idx_16645_fk1c41a80dfa349234; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16645_fk1c41a80dfa349234 ON process_result USING btree (id_process_result_parent);


--
-- Name: idx_16645_id_test; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16645_id_test ON process_result USING btree (id_test, id_web_resource, id_audit_gross_result);


--
-- Name: idx_16645_id_test_2; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16645_id_test_2 ON process_result USING btree (id_test, id_web_resource, id_audit_net_result);


--
-- Name: idx_16655_fk5411075edf74e053; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16655_fk5411075edf74e053 ON process_result_aud USING btree (rev);


--
-- Name: idx_16666_cd_reference_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16666_cd_reference_unique ON reference USING btree (cd_reference);


--
-- Name: idx_16666_fk_ref_level; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16666_fk_ref_level ON reference USING btree (id_default_level);


--
-- Name: idx_16679_unique_index_tgsi_referential_code; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16679_unique_index_tgsi_referential_code ON referential USING btree (code);


--
-- Name: idx_16696_index_tgsi_role_role_id_role; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16696_index_tgsi_role_role_id_role ON role USING btree (role_id_role);


--
-- Name: idx_16702_index_tgsi_scenario_contract_id_contract; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16702_index_tgsi_scenario_contract_id_contract ON scenario USING btree (contract_id_contract);


--
-- Name: idx_16735_value_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16735_value_unique ON tag USING btree (value);


--
-- Name: idx_16742_cd_test_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16742_cd_test_unique ON test USING btree (cd_test);


--
-- Name: idx_16742_fk273c9250c99824; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16742_fk273c9250c99824 ON test USING btree (id_scope);


--
-- Name: idx_16742_fk273c926cca4c3e; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16742_fk273c926cca4c3e ON test USING btree (id_criterion);


--
-- Name: idx_16742_fk273c9272343a84; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16742_fk273c9272343a84 ON test USING btree (id_level);


--
-- Name: idx_16742_fk273c92cca757ad; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16742_fk273c92cca757ad ON test USING btree (id_decision_level);


--
-- Name: idx_16758_fk_theme_statistics_theme; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16758_fk_theme_statistics_theme ON test_statistics USING btree (id_test);


--
-- Name: idx_16758_fk_theme_statistics_web_resource_statistics; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16758_fk_theme_statistics_web_resource_statistics ON test_statistics USING btree (id_web_resource_statistics);


--
-- Name: idx_16764_cd_theme_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16764_cd_theme_index ON theme USING btree (cd_theme);


--
-- Name: idx_16764_cd_theme_unique; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16764_cd_theme_unique ON theme USING btree (cd_theme);


--
-- Name: idx_16775_fk_theme_statistics_theme; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16775_fk_theme_statistics_theme ON theme_statistics USING btree (id_theme);


--
-- Name: idx_16775_fk_theme_statistics_web_resource_statistics; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16775_fk_theme_statistics_web_resource_statistics ON theme_statistics USING btree (id_web_resource_statistics);


--
-- Name: idx_16781_index_tgsi_user_role_id_role; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16781_index_tgsi_user_role_id_role ON "user" USING btree (role_id_role);


--
-- Name: idx_16781_unique_index_tgsi_user_email1; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16781_unique_index_tgsi_user_email1 ON "user" USING btree (email1);


--
-- Name: idx_16797_fk_tgsi_user_option_element_tgsi_user; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16797_fk_tgsi_user_option_element_tgsi_user ON user_option_element USING btree (user_id_user);


--
-- Name: idx_16797_index_unique_pair; Type: INDEX; Schema: asqatasun;
--

CREATE UNIQUE INDEX idx_16797_index_unique_pair ON user_option_element USING btree (option_element_id_option_element, user_id_user);


--
-- Name: idx_16802_dtype_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16802_dtype_index ON web_resource USING btree (dtype);


--
-- Name: idx_16802_fkd9a970b92f70ff12; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16802_fkd9a970b92f70ff12 ON web_resource USING btree (id_web_resource_parent);


--
-- Name: idx_16802_fkd9a970b9493ec9c2; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16802_fkd9a970b9493ec9c2 ON web_resource USING btree (id_audit);


--
-- Name: idx_16802_url_index; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16802_url_index ON web_resource USING btree (url);


--
-- Name: idx_16813_fk_web_resource_statistics_audit; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16813_fk_web_resource_statistics_audit ON web_resource_statistics USING btree (id_audit);


--
-- Name: idx_16813_fk_web_resource_statistics_web_resource; Type: INDEX; Schema: asqatasun;
--

CREATE INDEX idx_16813_fk_web_resource_statistics_web_resource ON web_resource_statistics USING btree (id_web_resource);


--
-- Name: process_remark fk1c3ea37045a988ad; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_remark
    ADD CONSTRAINT fk1c3ea37045a988ad FOREIGN KEY (id_process_result) REFERENCES process_result(id_process_result) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: process_result fk1c41a80d2e48600; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT fk1c41a80d2e48600 FOREIGN KEY (id_web_resource) REFERENCES web_resource(id_web_resource) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: process_result fk1c41a80d8146180b; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT fk1c41a80d8146180b FOREIGN KEY (id_audit_gross_result) REFERENCES audit(id_audit) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: process_result fk1c41a80da17a5fa8; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT fk1c41a80da17a5fa8 FOREIGN KEY (id_test) REFERENCES test(id_test) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: process_result fk1c41a80db6d0e092; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT fk1c41a80db6d0e092 FOREIGN KEY (id_audit_net_result) REFERENCES audit(id_audit) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: process_result fk1c41a80dfa349234; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result
    ADD CONSTRAINT fk1c41a80dfa349234 FOREIGN KEY (id_process_result_parent) REFERENCES process_result(id_process_result) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: test fk273c9250c99824; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test
    ADD CONSTRAINT fk273c9250c99824 FOREIGN KEY (id_scope) REFERENCES scope(id_scope) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: test fk273c926cca4c3e; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test
    ADD CONSTRAINT fk273c926cca4c3e FOREIGN KEY (id_criterion) REFERENCES criterion(id_criterion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: test fk273c9272343a84; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test
    ADD CONSTRAINT fk273c9272343a84 FOREIGN KEY (id_level) REFERENCES level(id_level) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: test fk273c92cca757ad; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test
    ADD CONSTRAINT fk273c92cca757ad FOREIGN KEY (id_decision_level) REFERENCES decision_level(id_decision_level) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nomenclature_element fk44f856145fab5ef2; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature_element
    ADD CONSTRAINT fk44f856145fab5ef2 FOREIGN KEY (id_nomenclature) REFERENCES nomenclature(id_nomenclature) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: process_result_aud fk5411075edf74e053; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY process_result_aud
    ADD CONSTRAINT fk5411075edf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: content fk6382c059493ec9c2; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content
    ADD CONSTRAINT fk6382c059493ec9c2 FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: content fk6382c059a8a177a1; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content
    ADD CONSTRAINT fk6382c059a8a177a1 FOREIGN KEY (id_page) REFERENCES web_resource(id_web_resource) ON DELETE CASCADE;


--
-- Name: evidence_element fk698b98f425ad22c4; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence_element
    ADD CONSTRAINT fk698b98f425ad22c4 FOREIGN KEY (process_remark_id_process_remark) REFERENCES process_remark(id_process_remark) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: evidence_element fk698b98f4c94a0cba; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY evidence_element
    ADD CONSTRAINT fk698b98f4c94a0cba FOREIGN KEY (evidence_id_evidence) REFERENCES evidence(id_evidence) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: audit_test fk838e6e96493ec9c2; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_test
    ADD CONSTRAINT fk838e6e96493ec9c2 FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: audit_test fk838e6e96a17a5fa8; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_test
    ADD CONSTRAINT fk838e6e96a17a5fa8 FOREIGN KEY (id_test) REFERENCES test(id_test);


--
-- Name: audit_tag fk838e6e96abc9863d; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_tag
    ADD CONSTRAINT fk838e6e96abc9863d FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: audit_tag fk838e6e96f94b364a; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_tag
    ADD CONSTRAINT fk838e6e96f94b364a FOREIGN KEY (id_tag) REFERENCES tag(id_tag);


--
-- Name: audit_parameter fk_audit_parameter_audit; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_parameter
    ADD CONSTRAINT fk_audit_parameter_audit FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: audit_parameter fk_audit_parameter_parameter; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY audit_parameter
    ADD CONSTRAINT fk_audit_parameter_parameter FOREIGN KEY (id_parameter) REFERENCES parameter(id_parameter) ON DELETE CASCADE;


--
-- Name: criterion_statistics fk_criterion_statistics_criterion; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion_statistics
    ADD CONSTRAINT fk_criterion_statistics_criterion FOREIGN KEY (id_criterion) REFERENCES criterion(id_criterion) ON DELETE CASCADE;


--
-- Name: criterion_statistics fk_criterion_statistics_web_resource_statistics; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion_statistics
    ADD CONSTRAINT fk_criterion_statistics_web_resource_statistics FOREIGN KEY (id_web_resource_statistics) REFERENCES web_resource_statistics(id_web_resource_statistics) ON DELETE CASCADE;


--
-- Name: parameter_element fk_parameter_element_parameter; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter_element
    ADD CONSTRAINT fk_parameter_element_parameter FOREIGN KEY (id_parameter_family) REFERENCES parameter_family(id_parameter_family);


--
-- Name: parameter fk_parameter_parameter_element; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT fk_parameter_parameter_element FOREIGN KEY (id_parameter_element) REFERENCES parameter_element(id_parameter_element);


--
-- Name: pre_process_result fk_pre_process_result_audit; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY pre_process_result
    ADD CONSTRAINT fk_pre_process_result_audit FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: pre_process_result fk_pre_process_result_web_resource; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY pre_process_result
    ADD CONSTRAINT fk_pre_process_result_web_resource FOREIGN KEY (id_web_resource) REFERENCES web_resource(id_web_resource) ON DELETE CASCADE;


--
-- Name: reference fk_ref_level; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY reference
    ADD CONSTRAINT fk_ref_level FOREIGN KEY (id_default_level) REFERENCES level(id_level) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: test_statistics fk_test_statistics_test; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test_statistics
    ADD CONSTRAINT fk_test_statistics_test FOREIGN KEY (id_test) REFERENCES test(id_test) ON DELETE CASCADE;


--
-- Name: test_statistics fk_test_statistics_web_resource_statistics; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY test_statistics
    ADD CONSTRAINT fk_test_statistics_web_resource_statistics FOREIGN KEY (id_web_resource_statistics) REFERENCES web_resource_statistics(id_web_resource_statistics) ON DELETE CASCADE;


--
-- Name: act_audit fk_tgsi_act_audit_audit; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY act_audit
    ADD CONSTRAINT fk_tgsi_act_audit_audit FOREIGN KEY (audit_id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: act_audit fk_tgsi_act_audit_tgsi_act; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY act_audit
    ADD CONSTRAINT fk_tgsi_act_audit_tgsi_act FOREIGN KEY (act_id_act) REFERENCES act(id_act) ON DELETE CASCADE;


--
-- Name: act fk_tgsi_act_tgsi_contract; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY act
    ADD CONSTRAINT fk_tgsi_act_tgsi_contract FOREIGN KEY (contract_id_contract) REFERENCES contract(id_contract) ON DELETE CASCADE;


--
-- Name: act fk_tgsi_act_tgsi_scope; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY act
    ADD CONSTRAINT fk_tgsi_act_tgsi_scope FOREIGN KEY (scope_id_scope) REFERENCES audit_scope(id_scope);


--
-- Name: contract_functionality fk_tgsi_contract_functionality_tgsi_contract; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_functionality
    ADD CONSTRAINT fk_tgsi_contract_functionality_tgsi_contract FOREIGN KEY (contract_id_contract) REFERENCES contract(id_contract) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_functionality fk_tgsi_contract_functionality_tgsi_functionality; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_functionality
    ADD CONSTRAINT fk_tgsi_contract_functionality_tgsi_functionality FOREIGN KEY (functionality_id_functionality) REFERENCES functionality(id_functionality);


--
-- Name: contract_option_element fk_tgsi_contract_option_element_tgsi_contract; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_option_element
    ADD CONSTRAINT fk_tgsi_contract_option_element_tgsi_contract FOREIGN KEY (contract_id_contract) REFERENCES contract(id_contract) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_option_element fk_tgsi_contract_option_element_tgsi_option_element; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_option_element
    ADD CONSTRAINT fk_tgsi_contract_option_element_tgsi_option_element FOREIGN KEY (option_element_id_option_element) REFERENCES option_element(id_option_element) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_referential fk_tgsi_contract_referential_tgsi_contract; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_referential
    ADD CONSTRAINT fk_tgsi_contract_referential_tgsi_contract FOREIGN KEY (contract_id_contract) REFERENCES contract(id_contract) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_referential fk_tgsi_contract_referential_tgsi_referential; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract_referential
    ADD CONSTRAINT fk_tgsi_contract_referential_tgsi_referential FOREIGN KEY (referential_id_referential) REFERENCES referential(id_referential);


--
-- Name: contract fk_tgsi_contract_tgsi_user; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY contract
    ADD CONSTRAINT fk_tgsi_contract_tgsi_user FOREIGN KEY (user_id_user) REFERENCES "user"(id_user) ON DELETE CASCADE;


--
-- Name: option_element fk_tgsi_option_element_tgsi_option; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY option_element
    ADD CONSTRAINT fk_tgsi_option_element_tgsi_option FOREIGN KEY (option_id_option) REFERENCES option_value(id_option);


--
-- Name: option_value fk_tgsi_option_tgsi_option_family; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY option_value
    ADD CONSTRAINT fk_tgsi_option_tgsi_option_family FOREIGN KEY (option_family_id_option_family) REFERENCES option_family(id_option_family);


--
-- Name: role fk_tgsi_role_tgsi_role; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY role
    ADD CONSTRAINT fk_tgsi_role_tgsi_role FOREIGN KEY (role_id_role) REFERENCES role(id_role) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario fk_tgsi_scenario_tgsi_contract; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY scenario
    ADD CONSTRAINT fk_tgsi_scenario_tgsi_contract FOREIGN KEY (contract_id_contract) REFERENCES contract(id_contract) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_option_element fk_tgsi_user_option_element_tgsi_option_element; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY user_option_element
    ADD CONSTRAINT fk_tgsi_user_option_element_tgsi_option_element FOREIGN KEY (option_element_id_option_element) REFERENCES option_element(id_option_element) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_option_element fk_tgsi_user_option_element_tgsi_user; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY user_option_element
    ADD CONSTRAINT fk_tgsi_user_option_element_tgsi_user FOREIGN KEY (user_id_user) REFERENCES "user"(id_user) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user fk_tgsi_user_tgsi_role; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_tgsi_user_tgsi_role FOREIGN KEY (role_id_role) REFERENCES role(id_role);


--
-- Name: theme_statistics fk_theme_statistics_theme; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY theme_statistics
    ADD CONSTRAINT fk_theme_statistics_theme FOREIGN KEY (id_theme) REFERENCES theme(id_theme) ON DELETE CASCADE;


--
-- Name: theme_statistics fk_theme_statistics_web_resource_statistics; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY theme_statistics
    ADD CONSTRAINT fk_theme_statistics_web_resource_statistics FOREIGN KEY (id_web_resource_statistics) REFERENCES web_resource_statistics(id_web_resource_statistics) ON DELETE CASCADE;


--
-- Name: web_resource_statistics fk_web_resource_statistics_audit; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource_statistics
    ADD CONSTRAINT fk_web_resource_statistics_audit FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON DELETE CASCADE;


--
-- Name: web_resource_statistics fk_web_resource_statistics_web_resource; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource_statistics
    ADD CONSTRAINT fk_web_resource_statistics_web_resource FOREIGN KEY (id_web_resource) REFERENCES web_resource(id_web_resource) ON DELETE CASCADE;


--
-- Name: content_relationship fkba33205e620a8494; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content_relationship
    ADD CONSTRAINT fkba33205e620a8494 FOREIGN KEY (id_content_parent) REFERENCES content(id_content) ON DELETE CASCADE;


--
-- Name: content_relationship fkba33205eba71c750; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY content_relationship
    ADD CONSTRAINT fkba33205eba71c750 FOREIGN KEY (id_content_child) REFERENCES content(id_content) ON DELETE CASCADE;


--
-- Name: criterion fkbcfa1e81d03ce506; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion
    ADD CONSTRAINT fkbcfa1e81d03ce506 FOREIGN KEY (reference_id_reference) REFERENCES reference(id_reference) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: criterion fkbcfa1e81e8f67244; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY criterion
    ADD CONSTRAINT fkbcfa1e81e8f67244 FOREIGN KEY (theme_id_theme) REFERENCES theme(id_theme) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: nomenclature fkbf856b7795431825; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY nomenclature
    ADD CONSTRAINT fkbf856b7795431825 FOREIGN KEY (id_nomenclature_parent) REFERENCES nomenclature(id_nomenclature) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: web_resource fkd9a970b92f70ff12; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource
    ADD CONSTRAINT fkd9a970b92f70ff12 FOREIGN KEY (id_web_resource_parent) REFERENCES web_resource(id_web_resource) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: web_resource fkd9a970b9493ec9c2; Type: FK CONSTRAINT; Schema: asqatasun;
--

ALTER TABLE ONLY web_resource
    ADD CONSTRAINT fkd9a970b9493ec9c2 FOREIGN KEY (id_audit) REFERENCES audit(id_audit) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

