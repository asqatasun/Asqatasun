--
-- Data for Name: role; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO ROLE (id_role, role_name, role_id_role) VALUES (1, 'ROLE_GUEST', NULL);
INSERT INTO ROLE (id_role, role_name, role_id_role) VALUES (2, 'ROLE_USER', NULL);
INSERT INTO ROLE (id_role, role_name, role_id_role) VALUES (3, 'ROLE_ADMIN', 2);


--
-- Data for Name: user; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO USERS (Id_User, Email1, Password, Name, First_Name, Address, Phone_Number, Email2, Web1, Web2, Identica_Id, Twitter_Id, ROLE_Id_Role, Activated) VALUES (1, 'admin@asqatasun.org','7f940a93b40507f8ef641f86bca97e00', ' ', ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, true);



--
-- Data for Name: contract; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO CONTRACT (id_contract, label, begin_date, end_date, renewal_date, price, user_id_user) VALUES (1, 'OPEN BAR', '2023-09-18 00:00:00', '2026-09-18 23:59:59', NULL, NULL, 1);



--
-- Data for Name: functionality; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO functionality (id_functionality, code, label, description) VALUES (1, 'PAGES', 'Audit Pages (Up to 10)', '');
INSERT INTO functionality (id_functionality, code, label, description) VALUES (2, 'DOMAIN', 'Audit Domain ', '');
INSERT INTO functionality (id_functionality, code, label, description) VALUES (3, 'UPLOAD', 'Audit Upload', '');
INSERT INTO functionality (id_functionality, code, label, description) VALUES (4, 'SCENARIO', 'Audit Scenario', '');



--
-- Data for Name: contract_functionality; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO contract_functionality (contract_id_contract, functionality_id_functionality) VALUES (1, 1);
INSERT INTO contract_functionality (contract_id_contract, functionality_id_functionality) VALUES (1, 2);
INSERT INTO contract_functionality (contract_id_contract, functionality_id_functionality) VALUES (1, 3);
INSERT INTO contract_functionality (contract_id_contract, functionality_id_functionality) VALUES (1, 4);

--
-- Data for Name: option_family; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO option_family (id_option_family, code, label, description) VALUES (1, 'CRAWL', '', NULL);
INSERT INTO option_family (id_option_family, code, label, description) VALUES (2, 'ACT_RESTRICTION', '', NULL);
INSERT INTO option_family (id_option_family, code, label, description) VALUES (3, 'ACT_MANAGEMENT', '', NULL);
INSERT INTO option_family (id_option_family, code, label, description) VALUES (4, 'CONTRACT_MANAGEMENT', '', NULL);



--
-- Data for Name: option_value; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (1, 'ACT_LIMITATION', NULL, 'The act limitation unit is the number of acts', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (2, 'ACT_BY_IP_LIMITATION', NULL, 'The act by ip limitation unit is the number of acts in a period for a given ip.
The period is expressed in seconds and the format is "nb_of_acts/period"', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (3, 'MAX_DOCUMENTS', NULL, 'This restriction limits the max number of crawled documents', true, 1);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (4, 'FORDIDDEN_REFERENTIAL', NULL, 'This restriction forbids the access to a referential', true, 1);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (5, 'DEPTH', NULL, 'This restriction limits the depth of the crawl', true, 1);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (6, 'MAX_DURATION', NULL, 'This restriction limits the duration of the crawl', true, 1);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (7, 'EXCLUSION_REGEXP', NULL, 'This restriction applies an exclusion rule on crawled Urls', true, 1);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (8, 'ACT_LIFETIME', NULL, 'This restriction determines the lifetime of each associated with the contract', true, 3);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (9, 'NB_OF_AUDIT_TO_DISPLAY', NULL, 'This restriction determines the number of audit results that can be displayed on the contract page', true, 4);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (10, 'DOMAIN', NULL, 'Domain associated with a contract', true, 4);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (11, 'PAGES_ACT_LIMITATION', NULL, 'The number of authorized acts of pages scope', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (12, 'DOMAIN_ACT_LIMITATION', NULL, 'The number of authorized acts of domain scope', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (13, 'UPLOAD_ACT_LIMITATION', NULL, 'The number of authorized acts of upload scope', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (14, 'SCENARIO_ACT_LIMITATION', NULL, 'The number of authorized acts of scenario scope', true, 2);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (15, 'PRESET_CONTRACT', NULL, 'A preset contract', false, 4);
INSERT INTO option_value (id_option, code, label, description, is_restriction, option_family_id_option_family) VALUES (16, 'ROBOTS_TXT_ACTIVATION', NULL, 'This option determines whether the crawl respects the robots.txt directives', true, 1);



--
-- Data for Name: option_element; Type: TABLE DATA; Schema: asqatasun; Owner: asqatasunDatabaseUserLogin
--

INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (1, 1, '5');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (2, 2, '5/3600');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (3, 3, '100');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (4, 3, '10000');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (5, 3, '20000');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (7, 8, '-1');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (9, 8, '30');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (10, 8, '365');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (8, 8, '5');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (12, 9, '10');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (14, 9, '100');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (11, 9, '5');
INSERT INTO option_element (id_option_element, option_id_option, value) VALUES (13, 9, '50');


--
-- Data for Name: referential; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO referential (id_referential, code, label, description) VALUES (3, 'Rgaa40', 'Rgaa 4.0', NULL);
INSERT INTO referential (id_referential, code, label, description) VALUES (4, 'Seo', 'Seo referential', NULL);



--
-- Data for Name: contract_referential; Type: TABLE DATA; Schema: asqatasun;
--

INSERT INTO contract_referential (contract_id_contract, referential_id_referential) VALUES (1, 3);
INSERT INTO contract_referential (contract_id_contract, referential_id_referential) VALUES (1, 4);
