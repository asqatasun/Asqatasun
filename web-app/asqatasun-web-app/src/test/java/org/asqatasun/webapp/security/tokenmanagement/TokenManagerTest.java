package org.asqatasun.webapp.security.tokenmanagement;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class TokenManagerTest {

    private static final String USER = "asqa@asqatasun.org";
    @InjectMocks
    private TokenManager tokenManager;

    public void setTokenDurationValidity(int durationValidity) {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(tokenManager, "tokenDurationValidity", durationValidity);
    }

    @Test
    void checkTokenUserIntegrity() {
        setTokenDurationValidity(10);
        String token = tokenManager.getTokenUser(USER);
        assertEquals(USER, tokenManager.checkUserToken(token));
    }

    @Test
    void checkTokenUserExpiration() {
        setTokenDurationValidity(0);
        String token = tokenManager.getTokenUser(USER);
        JwtException exception = assertThrows(ExpiredJwtException.class, () -> {
            tokenManager.checkUserToken(token);
        });
        assertTrue(exception.getMessage().contains("Allowed clock skew: 0 milliseconds."));
    }

}
