<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- external JS --%>
<c:set var="jqueryMigrateUrl">
    <%-- Here we choose the **minified** version as we *don't* want to have the warnings displayed on the JS console --%>
    <c:url value="/public/external_js/jquery-migrate-3.4.1.min.js?v${asqatasunVersion}"/>
</c:set>
<c:set var="jqueryTableSorterUrl" scope="request">
    <c:url value="/public/external_js/jquery.tablesorter.min.js?v${asqatasunVersion}"/>
</c:set>

<%-- internal JS --%>
<c:set var="accessibleTableSorterJsUrl" scope="page">
    <c:url value="/public/js/table-sorter/accessible-table-sorter-min.js?v${asqatasunVersion}"/>
</c:set>

        <script type="text/javascript" src="${jqueryTableSorterUrl}"></script>
        <script type="text/javascript" src="${accessibleTableSorterJsUrl}"></script>
