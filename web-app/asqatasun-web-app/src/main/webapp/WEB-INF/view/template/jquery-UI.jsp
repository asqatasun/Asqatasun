<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:set var="jqueryUIUrl">
    <c:url value="/public/external_js/jquery-ui-1.13.3.min.js?v${asqatasunVersion}"/>
</c:set>

        <script type="text/javascript" src="${jqueryUIUrl}"></script>
