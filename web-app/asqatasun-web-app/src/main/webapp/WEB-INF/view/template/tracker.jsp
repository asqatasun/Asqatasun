<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<c:if test="${not empty matomoSiteId and not empty matomoUrl}">
    <!-- Matomo -->
    <script type="text/javascript">
        var matomoUrl = '${matomoUrl}';
        var matomoSiteId = '${matomoSiteId}';
        var _paq = window._paq = window._paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u=matomoUrl;
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', matomoSiteId]);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
    <!-- End Matomo Code -->
</c:if>
