$(document).ready(function() {
    
    $('#launch-audit-submit').on("click", function() {
        
        var launchProcessDialogTimeout = 10;
        setTimeout(launchProcessDialog,launchProcessDialogTimeout);
        
    });

});

function launchProcessDialog() {
    var processingMessages = [];
    var messageIndex = 0;

    $(".process-message").each(function (d, i) {
        processingMessages.push($(this).text());
        $(this).hide();
    });

    $("#process-anim").show();

    $("#process-dialog")
        .append("<p id='process-msg' aria-live='true'>"+processingMessages[messageIndex]+"</p>")
        .dialog({
            autoOpen : true,
            modal : true,
            title :  "",
            resizable : false,
            draggable : false,
            dialogClass : "noCloseBtn",
            width : 300
        });

    setInterval(function() {
        messageIndex++;
        $('#process-msg').text(processingMessages[messageIndex]);
    }, 1500); // = asynchronous delay (30s) / nb of messages (20)
}
