/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.webapp.config.ui;

import org.asqatasun.webapp.ui.form.builder.SelectElementBuilderImpl;
import org.asqatasun.webapp.ui.form.builder.SelectFormFieldBuilderImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;

@Configuration
public class ReferentialConfiguration {

    @Bean
    public SelectFormFieldBuilderImpl levelSelectFormFieldBuilder() {
        return ContractFormElementConfiguration.buildSelectFormFieldBuilder(
            "level",
            "level-error",
            new HashMap <>() {{
                put("Rgaa412", Arrays.asList(
                    rgaa412ASelectElementBuilder(),
                    rgaa412AASelectElementBuilder()
                ));
                put("Rgaa40", Arrays.asList(
                    rgaa40ASelectElementBuilder(),
                    rgaa40AASelectElementBuilder()
                ));
                put("Seo", Arrays.asList(
                    seoASelectElementBuilder(),
                    seoAASelectElementBuilder(),
                    seoAAASelectElementBuilder()
                ));
            }}
        );
    }

    @Bean
    public SelectElementBuilderImpl rgaa412ASelectElementBuilder(){
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412;LEVEL_1",
            "Rgaa412-LEVEL_1",
            "A-error",
            "DEFAULT_LEVEL",
            false,
            false
        );
    }

    @Bean
    public SelectElementBuilderImpl rgaa412AASelectElementBuilder(){
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412;LEVEL_2",
            "Rgaa412-LEVEL_2",
            "LEVEL_2-error",
            "DEFAULT_LEVEL",
            false,
            false
        );
    }

    @Bean
    public SelectFormFieldBuilderImpl rgaa412ThemeSelectFormFieldBuilder() {
        return ContractFormElementConfiguration.buildSelectFormFieldBuilder(
            "result.theme",
            "result.theme-error",
            new HashMap <>() {{
                put("theme", Arrays.asList(
                    rgaa412AllThemeSelectElementBuilder(),
                    rgaa4121ThemeSelectElementBuilder(),
                    rgaa4122ThemeSelectElementBuilder(),
                    rgaa4123ThemeSelectElementBuilder(),
                    rgaa4124ThemeSelectElementBuilder(),
                    rgaa4125ThemeSelectElementBuilder(),
                    rgaa4126ThemeSelectElementBuilder(),
                    rgaa4127ThemeSelectElementBuilder(),
                    rgaa4128ThemeSelectElementBuilder(),
                    rgaa4129ThemeSelectElementBuilder(),
                    rgaa41210ThemeSelectElementBuilder(),
                    rgaa41211ThemeSelectElementBuilder(),
                    rgaa41212ThemeSelectElementBuilder(),
                    rgaa41213ThemeSelectElementBuilder()
                ));
            }},
            "Rgaa412");
    }

    @Bean
    public SelectElementBuilderImpl rgaa412AllThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "all-theme",
            "result.all-theme",
            "result.all-theme-error",
            true,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4121ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-1",
            "Rgaa412-1",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4122ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-2",
            "Rgaa412-2",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4123ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-3",
            "Rgaa412-3",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4124ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-4",
            "Rgaa412-4",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4125ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-5",
            "Rgaa412-5",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4126ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-6",
            "Rgaa412-6",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4127ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-7",
            "Rgaa412-7",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4128ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-8",
            "Rgaa412-8",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4129ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-9",
            "Rgaa412-9",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa41210ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-10",
            "Rgaa412-10",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa41211ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-11",
            "Rgaa412-11",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa41212ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-12",
            "Rgaa412-12",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa41213ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa412-13",
            "Rgaa412-13",
            "",
            false,
            true);
    }

    // Rgaa 40 select element builder
    @Bean
    public SelectElementBuilderImpl rgaa40ASelectElementBuilder(){
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40;LEVEL_1",
            "Rgaa40-LEVEL_1",
            "A-error",
            "DEFAULT_LEVEL",
            false,
            false
        );
    }
    @Bean
    public SelectElementBuilderImpl rgaa40AASelectElementBuilder(){
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40;LEVEL_2",
            "Rgaa40-LEVEL_2",
            "LEVEL_2-error",
            "DEFAULT_LEVEL",
            false,
            false
        );
    }

    @Bean
    public SelectFormFieldBuilderImpl rgaa40ThemeSelectFormFieldBuilder() {
        return ContractFormElementConfiguration.buildSelectFormFieldBuilder(
            "result.theme",
            "result.theme-error",
            new HashMap <>() {{
                put("theme", Arrays.asList(
                    rgaa40AllThemeSelectElementBuilder(),
                    rgaa401ThemeSelectElementBuilder(),
                    rgaa402ThemeSelectElementBuilder(),
                    rgaa403ThemeSelectElementBuilder(),
                    rgaa404ThemeSelectElementBuilder(),
                    rgaa405ThemeSelectElementBuilder(),
                    rgaa406ThemeSelectElementBuilder(),
                    rgaa407ThemeSelectElementBuilder(),
                    rgaa408ThemeSelectElementBuilder(),
                    rgaa409ThemeSelectElementBuilder(),
                    rgaa4010ThemeSelectElementBuilder(),
                    rgaa4011ThemeSelectElementBuilder(),
                    rgaa4012ThemeSelectElementBuilder(),
                    rgaa4013ThemeSelectElementBuilder()
                ));
            }},
            "Rgaa40");
    }
    @Bean
    public SelectElementBuilderImpl rgaa40AllThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "all-theme",
            "result.all-theme",
            "result.all-theme-error",
            true,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa401ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-1",
            "Rgaa40-1",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa402ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-2",
            "Rgaa40-2",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa403ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-3",
            "Rgaa40-3",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa404ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-4",
            "Rgaa40-4",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa405ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-5",
            "Rgaa40-5",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa406ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-6",
            "Rgaa40-6",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa407ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-7",
            "Rgaa40-7",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa408ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-8",
            "Rgaa40-8",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa409ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-9",
            "Rgaa40-9",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4010ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-10",
            "Rgaa40-10",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4011ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-11",
            "Rgaa40-11",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4012ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-12",
            "Rgaa40-12",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl rgaa4013ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Rgaa40-13",
            "Rgaa40-13",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seoASelectElementBuilder(){
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo;LEVEL_1",
            "Seo-LEVEL_1",
            "LEVEL_1-error",
            "DEFAULT_LEVEL",
            false,
            false
        );
    }

    @Bean
    public SelectElementBuilderImpl seoAASelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo;LEVEL_2",
            "Seo-LEVEL_2",
            "LEVEL_2-error",
            "DEFAULT_LEVEL",
            true,
            false
        );
    }

    @Bean
    public SelectElementBuilderImpl seoAAASelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo;LEVEL_3",
            "Seo-LEVEL_3",
            "LEVEL_3-error",
            "DEFAULT_LEVEL",
            true,
            false
        );
    }

    @Bean
    public SelectFormFieldBuilderImpl seoThemeSelectFormFieldBuilder() {
        return ContractFormElementConfiguration.buildSelectFormFieldBuilder(
            "result.theme",
            "result.theme-error",
            new HashMap <>() {{
                put("theme", Arrays.asList(
                    seoAllThemeSelectElementBuilder(),
                    seo01ThemeSelectElementBuilder(),
                    seo02ThemeSelectElementBuilder(),
                    seo03ThemeSelectElementBuilder(),
                    seo04ThemeSelectElementBuilder(),
                    seo05ThemeSelectElementBuilder(),
                    seo06ThemeSelectElementBuilder(),
                    seo07ThemeSelectElementBuilder()
                ));
            }},
            "Seo");
    }

    @Bean
    public SelectElementBuilderImpl seoAllThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "all-theme",
            "result.all-theme",
            "result.all-theme-error",
            true,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo01ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-01",
            "Seo-01",
            "",
            false,
            true);
    }
    @Bean
    public SelectElementBuilderImpl seo02ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-02",
            "Seo-02",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo03ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-03",
            "Seo-03",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo04ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-04",
            "Seo-04",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo05ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-05",
            "Seo-05",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo06ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-06",
            "Seo-06",
            "",
            false,
            true);
    }

    @Bean
    public SelectElementBuilderImpl seo07ThemeSelectElementBuilder() {
        return ContractFormElementConfiguration.buildSelectElementBuilder(
            "Seo-07",
            "Seo-07",
            "",
            false,
            true);
    }
}
