 /*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.webapp.controller;

 import io.jsonwebtoken.JwtException;
 import jakarta.servlet.http.HttpServletRequest;
 import org.apache.commons.lang3.StringUtils;
 import org.asqatasun.emailsender.EmailSender;
 import org.asqatasun.entity.user.User;
 import org.asqatasun.util.MD5Encoder;
 import org.asqatasun.webapp.command.ChangePasswordCommand;
 import org.asqatasun.webapp.command.ForgottenPasswordCommand;
 import org.asqatasun.webapp.exception.ForbiddenPageException;
 import org.asqatasun.webapp.security.tokenmanagement.TokenManager;
 import org.asqatasun.webapp.ui.form.menu.SecondaryLevelMenuDisplayer;
 import org.asqatasun.webapp.validator.ChangePasswordFormValidator;
 import org.asqatasun.webapp.validator.ForgottenPasswordFormValidator;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.beans.factory.annotation.Value;
 import org.springframework.context.support.ReloadableResourceBundleMessageSource;
 import org.springframework.security.access.annotation.Secured;
 import org.springframework.stereotype.Controller;
 import org.springframework.ui.Model;
 import org.springframework.validation.BindingResult;
 import org.springframework.web.bind.annotation.ModelAttribute;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestMethod;
 import org.springframework.web.bind.annotation.RequestParam;
 import org.springframework.web.servlet.LocaleResolver;

 import java.net.URLEncoder;
 import java.nio.charset.StandardCharsets;
 import java.util.*;

 import static org.asqatasun.webapp.util.TgolKeyStore.*;

/** 
 *
 * @author jkowalczyk
 */
@Controller
public class ForgottenOrChangePasswordController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ForgottenOrChangePasswordController.class);
    private static final String USER_ACCOUNT_TO_REPLACE =  "#userAccount";
    private static final String CHANGE_PASSWORD_URL_TO_REPLACE =  "#changePasswordUrl";
    private static final String CHANGE_PASSWORD_URL = "change-password";
    @Value("${app.emailSender.smtp.from}")
    private String appEmailFrom;
    @Value("${app.webapp.ui.config.forgottenPassword.excludeUserList}")
    private List<String> forbiddenUserList;
    @Value("${app.webapp.ui.config.webAppUrl}")
    private String webAppUrl;
    private final ChangePasswordFormValidator changePasswordFormValidator;
    private final ForgottenPasswordFormValidator forgottenPasswordFormValidator;
    private final EmailSender emailSender;
    private final LocaleResolver localeResolver;
    private final SecondaryLevelMenuDisplayer secondaryLevelMenuDisplayer;
    private final ReloadableResourceBundleMessageSource messageSource;
    private final TokenManager tokenManager;



    @Autowired
    public ForgottenOrChangePasswordController(
        ChangePasswordFormValidator changePasswordFormValidator,
        ForgottenPasswordFormValidator forgottenPasswordFormValidator,
        EmailSender emailSender, LocaleResolver localeResolver,
        SecondaryLevelMenuDisplayer secondaryLevelMenuDisplayer,
        ReloadableResourceBundleMessageSource messageSource,
        TokenManager tokenManager) {
        super();
        this.changePasswordFormValidator = changePasswordFormValidator;
        this.forgottenPasswordFormValidator = forgottenPasswordFormValidator;
        this.emailSender = emailSender;
        this.localeResolver = localeResolver;
        this.secondaryLevelMenuDisplayer = secondaryLevelMenuDisplayer;
        this.messageSource = messageSource;
        this.tokenManager = tokenManager;
    }

    /**
     * This method displays the change password page from an authenticated user
     * @param token
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = CHANGE_PASSWORD_URL, method = RequestMethod.GET)
    public String displayChangePasswordFromUserPage(
            @RequestParam(required = false) String token,
            HttpServletRequest request,
            Model model) {
        model.addAttribute(CHANGE_PASSWORD_FROM_ADMIN_KEY, false);
        secondaryLevelMenuDisplayer.setModifiableReferentialsForUserToModel(
                        getCurrentUser(), 
                        model);
        User user;
        // if the page is displayed from an authenticated context, no token is provided and the current User of the
        // session is used
        if (token == null) {
            user = getCurrentUser();
        // if a token is provided, we are within the "forgotten password" workflow which means no user is currently
        // authenticated, we extract the targeted user from the token, if the token is valid (signature, expiration)
        } else {
            try {
                String userEmail = tokenManager.checkUserToken(token);
                user = userDataService.getUserFromEmail(userEmail);
                if (model.asMap().containsKey(PASSWORD_MODIFIED_KEY) &&
                    (Boolean) model.asMap().get(PASSWORD_MODIFIED_KEY)) {
                    tokenManager.setTokenUsed(token);
                    return CHANGE_PASSWORD_VIEW_NAME;
                }
            } catch (ArrayIndexOutOfBoundsException | JwtException exception) {
                LOGGER.info(exception.getMessage());
                model.addAttribute(INVALID_CHANGE_PASSWORD_URL_KEY, true);
                return CHANGE_PASSWORD_VIEW_NAME;
            }
        }
        if (forbiddenUserList.contains(user.getEmail1())) {
            return ACCESS_DENIED_VIEW_REDIRECT_NAME;
        }
        return displayChangePasswordView(user, model, request);
    }

    /**
     * 
     * @param id
     * @param request
     * @param model
     * @return 
     */
    @RequestMapping(value = CHANGE_PASSWORD_FROM_ADMIN_URL, method = RequestMethod.GET)
    @Secured(ROLE_ADMIN_KEY)
    public String displayChangePasswordFromAdminPage(
            @RequestParam(USER_ID_KEY) Long id,
            HttpServletRequest request,
            Model model) {
        model.addAttribute(CHANGE_PASSWORD_FROM_ADMIN_KEY, true);
        User user = userDataService.read(id);
        if (getCurrentUser() == null ||
                !getCurrentUser().getRole().getRoleName().equals(ROLE_ADMIN_NAME_KEY)) {
            return ACCESS_DENIED_VIEW_REDIRECT_NAME;
        }
        return displayChangePasswordView(user, model, request);
    }
    
    /**
     * 
     * @param user
     * @param model
     * @param request
     * @return 
     */
    private String displayChangePasswordView(
            User user,
            Model model,
            HttpServletRequest request) {
        if (user == null) {
            return ACCESS_DENIED_VIEW_REDIRECT_NAME;
        }
        ChangePasswordCommand cpc = new ChangePasswordCommand();
        model.addAttribute(CHANGE_PASSWORD_COMMAND_KEY, cpc);
        model.addAttribute(USER_NAME_KEY, user.getEmail1());
        request.getSession().setAttribute(USER_ID_KEY,user.getId());
        return CHANGE_PASSWORD_VIEW_NAME;
    }
    
    /**
     * This method displays the change password page from an authenticated user
     * @param model
     * @return
     */
    @RequestMapping(value = FORGOTTEN_PASSWORD_URL, method = RequestMethod.GET)
    public String displayForgottenPasswordPage(
            Model model) {
        if (getCurrentUser() != null) {
            return ACCESS_DENIED_VIEW_REDIRECT_NAME;
        }
        model.addAttribute(FORGOTTEN_PASSWORD_COMMAND_KEY,
                new ForgottenPasswordCommand());
        return FORGOTTEN_PASSWORD_VIEW_NAME;
    }

    /**
     * This methods controls the validity of the form and modify the password 
     * of the wished user
     * 
     * @param forgottenPasswordCommand
     * @param result
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = FORGOTTEN_PASSWORD_URL, method = RequestMethod.POST)
    protected String submitForgottenPasswordForm(
            @ModelAttribute(FORGOTTEN_PASSWORD_COMMAND_KEY) ForgottenPasswordCommand forgottenPasswordCommand,
            BindingResult result,
            Model model,
            HttpServletRequest request) throws Exception {
        // We check whether the form is valid
        forgottenPasswordFormValidator.validate(forgottenPasswordCommand, result);
        // If the form has some errors, we display it again with errors' details
        if (result.hasErrors()) {
            return displayFormWithErrors(
                    model,
                    forgottenPasswordCommand);
        }
        Locale locale = localeResolver.resolveLocale(request);
        sendResetEmail(userDataService.getUserFromEmail(forgottenPasswordCommand.getEmail()), locale);
        request.getSession().setAttribute(URL_KEY, forgottenPasswordCommand.getEmail());
        return FORGOTTEN_PASSWORD_CONFIRMATION_VIEW_REDIRECT_NAME;
    }

    /**
     * 
     * @param changePasswordCommand
     * @param result
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = CHANGE_PASSWORD_URL, method = RequestMethod.POST)
    protected String submitChangePasswordFromUser(
            @ModelAttribute(CHANGE_PASSWORD_COMMAND_KEY) ChangePasswordCommand changePasswordCommand,
            BindingResult result,
            Model model,
            HttpServletRequest request)
            throws Exception {
        return changePassword(
                changePasswordCommand, 
                result, 
                model, 
                request,
                false);
    }
    
    /**
     * 
     * @param changePasswordCommand
     * @param result
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = CHANGE_PASSWORD_FROM_ADMIN_URL, method = RequestMethod.POST)
    @Secured(ROLE_ADMIN_KEY)
    protected String submitChangePasswordFromAdmin(
            @ModelAttribute(CHANGE_PASSWORD_COMMAND_KEY) ChangePasswordCommand changePasswordCommand,
            BindingResult result,
            Model model,
            HttpServletRequest request)
            throws Exception {
        return changePassword(
                changePasswordCommand, 
                result, 
                model, 
                request,
                true);
    }
    
    /**
     * 
     * @param changePasswordCommand
     * @param result
     * @param model
     * @param request
     * @param isRequestFromAdmin
     * @return
     * @throws Exception
     */
    protected String changePassword(
            ChangePasswordCommand changePasswordCommand,
            BindingResult result,
            Model model,
            HttpServletRequest request,
            boolean isRequestFromAdmin)
            throws Exception {
        User user = userDataService.read((Long)request.getSession().getAttribute(USER_ID_KEY));
        if (forbiddenUserList.contains(user.getEmail1())) {
            throw new ForbiddenPageException();
        }
        // We check whether the form is valid
        changePasswordFormValidator.validate(changePasswordCommand,result,user);
        // If the form has some errors, we display it again with errors' details
        if (result.hasErrors()) {
            model.addAttribute(USER_NAME_KEY, user.getEmail1());
            return displayChangePasswordFormWithErrors(
                    model,
                    changePasswordCommand,
                    isRequestFromAdmin);
        }
        request.getSession().removeAttribute(USER_ID_KEY);
        model.addAttribute(PASSWORD_MODIFIED_KEY, true);
        updateUserPassword(user,changePasswordCommand);
        if (isRequestFromAdmin) {
            return displayChangePasswordFromAdminPage(user.getId(), request, model);
        } else {
            return displayChangePasswordFromUserPage(request.getParameter("token"), request, model);
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = FORGOTTEN_PASSWORD_CONFIRMATION_URL, method = RequestMethod.GET)
    public String displayUpSignUpConfirmationPage(
            Model model,
            HttpServletRequest request) {
        model.addAttribute(URL_KEY, request.getSession().getAttribute(URL_KEY));
        request.getSession().removeAttribute(URL_KEY);
        return FORGOTTEN_PASSWORD_CONFIRMATION_VIEW_NAME;
    }

    /**
     * 
     * @param model
     * @param forgottenPasswordCommand
     * @return
     */
    private String displayFormWithErrors(
            Model model,
            ForgottenPasswordCommand forgottenPasswordCommand) {
        model.addAttribute(FORGOTTEN_PASSWORD_COMMAND_KEY,
                forgottenPasswordCommand);
        return FORGOTTEN_PASSWORD_VIEW_NAME;
    }
    
    /**
     *
     * @param model
     * @param changePasswordCommand
     * @param isrequestFromAdmin
     * @return
     */
    private String displayChangePasswordFormWithErrors(
            Model model,
            ChangePasswordCommand changePasswordCommand,
            boolean isrequestFromAdmin) {
        model.addAttribute(CHANGE_PASSWORD_COMMAND_KEY,
                changePasswordCommand);
        if (isrequestFromAdmin) {
            model.addAttribute(CHANGE_PASSWORD_FROM_ADMIN_KEY, true);
        } else {
            model.addAttribute(CHANGE_PASSWORD_FROM_ADMIN_KEY, false);
        }
        return CHANGE_PASSWORD_VIEW_NAME;
    }

    /**
     * This method gets data from a property file to fill-in the inscription
     * e-mail
     * @param user
     */
    private void sendResetEmail(User user, Locale locale) {
        Set<String> emailToSet = new HashSet<>();
        emailToSet.add(user.getEmail1());
        String emailSubject = messageSource.getMessage(FORGOTTEN_PASSWD_EMAIL_SUBJECT_KEY, null, locale);
        String emailContent = messageSource.getMessage(FORGOTTEN_PASSWD_EMAIL_CONTENT_KEY, null, locale).
            replaceAll(USER_ACCOUNT_TO_REPLACE, user.getEmail1()).
            replaceAll(CHANGE_PASSWORD_URL_TO_REPLACE, computeReturnedUrl(user));
        emailSender.sendEmail(
            appEmailFrom,
            emailToSet,
            Collections.<String>emptySet(),
            StringUtils.EMPTY,
            emailSubject,
            emailContent
        );
    }

    /**
     * 
     * @param user
     * @return
     */
    private String computeReturnedUrl(User user) {
        StringBuilder sb = new StringBuilder();
        sb.append(webAppUrl).append(CHANGE_PASSWORD_URL);
        sb.append("?token=");
        sb.append(URLEncoder.encode(tokenManager.getTokenUser(user.getEmail1()), StandardCharsets.UTF_8));
        return sb.toString();
    }

    /**
     *
     * @param user
     * @param changePasswordCommand
     * @return an User
     * @throws Exception
     */
    private User updateUserPassword(
            User user,
            ChangePasswordCommand changePasswordCommand) throws Exception {
        user.setPassword(MD5Encoder.MD5(changePasswordCommand.getNewPassword()));
        userDataService.saveOrUpdate(user);
        return user;
    }

}
