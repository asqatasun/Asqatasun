/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.webapp.config;

import jakarta.servlet.DispatcherType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

/**
 * Created by meskoj on 25/05/16.
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
//        MvcRequestMatcher.Builder builder = new MvcRequestMatcher.Builder(introspector).servletPath("/");
        http
            .csrf(AbstractHttpConfigurer::disable)
            .authorizeHttpRequests(auth -> auth
                .dispatcherTypeMatchers(DispatcherType.FORWARD).permitAll()
                .requestMatchers(
                    antMatcher("/"),
                    antMatcher("/login"),
                    antMatcher("/forgotten-password"),
                    antMatcher("/change-password"),
                    antMatcher("/access-denied"),
                    antMatcher("/actuator"),
                    antMatcher("/actuator/*/**"),
                    antMatcher("/forgotten-password-confirmation")).permitAll()
                .requestMatchers(
                    antMatcher("/dispatch"),
                    antMatcher("/home"),
                    antMatcher("/home/**")).hasAnyRole("USER", "ADMIN")
                .requestMatchers(
                    antMatcher("/admin"),
                    antMatcher("/admin/**")).hasAnyRole("ADMIN")
                .anyRequest().authenticated())
            // Whereas the documentation encourages to use MvcRequestMatcher, it leads to infinite redirect loop when
            // trying to render jsp pages.
            // Furthermore, as soon as this issue is not fixed (https://github.com/spring-projects/spring-security/issues/13568)
            // we'll use the ant matchers to fit with the previous implementation
//                .requestMatchers(
//                    builder.pattern("/"),
//                    builder.pattern("/login"),
//                    builder.pattern("/forgotten-password")).permitAll()
//                .requestMatchers(
//                    builder.pattern("/dispatch"),
//                    builder.pattern("/home"),
//                    builder.pattern("/home/**")).hasAnyRole("USER", "ADMIN")
//                .requestMatchers(
//                    builder.pattern("/admin"),
//                    builder.pattern("/admin/**")).hasAnyRole("ADMIN"))
            .formLogin( formLogin -> formLogin
                .usernameParameter("username") /* BY DEFAULT IS username!!! */
                .passwordParameter("password") /* BY DEFAULT IS password!!! */
                .loginProcessingUrl("/login")
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?error=errorOnLogin"))
            .logout( logout -> logout
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(false))
            .sessionManagement(sessionManagement -> sessionManagement
                .maximumSessions(100).expiredUrl("/login"))
            .exceptionHandling((exceptionHandling) -> exceptionHandling.accessDeniedPage("/access-denied"));
        return http.build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers(
            antMatcher("/favicon.ico"),
            antMatcher("/static/**"),
            antMatcher("/public/**"));
    }

}
