/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.webapp.security.tokenmanagement;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jkowalczyk
 */
@Service("tokenManager")
public final class TokenManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenManager.class);

    SecretKey secretKey;

    @Value("${app.webapp.security.jwt.tokenDurationValidity:600}")
    private int tokenDurationValidity;

    private final Map<String, Boolean> tokenUsage = new HashMap<>();

    /**
     * Default constructor
     */
    public TokenManager() {
        secretKey = Jwts.SIG.HS256.key().build();
    }
    
    /**
     *
     * @param userAccountName
     * @return
     */
    public String getTokenUser(String userAccountName) {
        return Jwts.builder()
            .header()
            .keyId("Asqatasun")
            .and()
            .subject(userAccountName)
            .signWith(secretKey)
            .expiration(
                Date.from(LocalDateTime.now().plusSeconds(tokenDurationValidity)
                    .atZone(ZoneId.systemDefault()).toInstant()))
            .compact();
    }

    /**
     *
     * @param token
     * @return the subject
     */
    public String checkUserToken(String token) throws JwtException{
        return Jwts.parser()
            .verifyWith(secretKey)
            .build()
            .parseSignedClaims(token).getPayload().getSubject();
    }

    public void setTokenUsed(String token) {
        tokenUsage.put(token, Boolean.TRUE);
    }
}
